#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=src/ADC/adc.c src/ADC/adc_isr.S src/COMMON/delay.c src/COMMON/func.c src/COMMON/timer.c src/COMMON/tiny_sprintf.c src/COMMON/timer_isr.S src/GPS/gps.c src/GPS/gps_parser.c src/JPEG/ImageDecoder.c src/JPEG/InternalFlashJPEGPicturesC32.c src/JPEG/jidctint.c src/JPEG/jpegData.c src/JPEG/JpegDecoder.c src/JPEG/JPEGImage.c src/LCD/LCD_DispMode/displayBody.c src/LCD/LCD_DispMode/displayMode_current.c src/LCD/LCD_DispMode/displayMode_draw.c src/LCD/LCD_DispMode/displayMode_gps.c src/LCD/LCD_DispMode/displayMode_graph.c src/LCD/LCD_DispMode/displayMode_nomal.c src/LCD/LCD_DispMode/displayMode_picture.c src/LCD/LCD_DispMode/displayMode_speed.c src/LCD/LCD_DispMode/displayMode_volt.c src/LCD/LCD_DispMode/displayMode_menu.c src/LCD/LCD_DispMode/displayTaskBar.c src/LCD/LCD_Lib/colorlcd_libdsPICVH.c src/LCD/LCD_Lib/digitalMeter.c src/LCD/LCD_Lib/displayControl.c src/LCD/LCD_Lib/displayGraph.c src/LCD/displayMode.c src/LCD/lcd_touch.c src/PORT/port.c src/PORT/port_isr.S src/SD/sd.c src/SD/skSDlib.c src/SD/skSPIlib.c ../Source/portable/MemMang/heap_1.c ../Source/portable/port.c ../Source/portable/port_asm.S ../Source/croutine.c ../Source/list.c ../Source/queue.c ../Source/tasks.c ../Source/event_groups.c ../Source/timers.c src/SYSTEM/ConfigPerformance.c src/SYSTEM/FreeRTOSHook.c src/SYSTEM/pic32Config.c src/UART/serial.c src/UART/serial_isr.S src/main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/src/ADC/adc.o ${OBJECTDIR}/src/ADC/adc_isr.o ${OBJECTDIR}/src/COMMON/delay.o ${OBJECTDIR}/src/COMMON/func.o ${OBJECTDIR}/src/COMMON/timer.o ${OBJECTDIR}/src/COMMON/tiny_sprintf.o ${OBJECTDIR}/src/COMMON/timer_isr.o ${OBJECTDIR}/src/GPS/gps.o ${OBJECTDIR}/src/GPS/gps_parser.o ${OBJECTDIR}/src/JPEG/ImageDecoder.o ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o ${OBJECTDIR}/src/JPEG/jidctint.o ${OBJECTDIR}/src/JPEG/jpegData.o ${OBJECTDIR}/src/JPEG/JpegDecoder.o ${OBJECTDIR}/src/JPEG/JPEGImage.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o ${OBJECTDIR}/src/LCD/displayMode.o ${OBJECTDIR}/src/LCD/lcd_touch.o ${OBJECTDIR}/src/PORT/port.o ${OBJECTDIR}/src/PORT/port_isr.o ${OBJECTDIR}/src/SD/sd.o ${OBJECTDIR}/src/SD/skSDlib.o ${OBJECTDIR}/src/SD/skSPIlib.o ${OBJECTDIR}/_ext/38565645/heap_1.o ${OBJECTDIR}/_ext/339404384/port.o ${OBJECTDIR}/_ext/339404384/port_asm.o ${OBJECTDIR}/_ext/1728301206/croutine.o ${OBJECTDIR}/_ext/1728301206/list.o ${OBJECTDIR}/_ext/1728301206/queue.o ${OBJECTDIR}/_ext/1728301206/tasks.o ${OBJECTDIR}/_ext/1728301206/event_groups.o ${OBJECTDIR}/_ext/1728301206/timers.o ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o ${OBJECTDIR}/src/SYSTEM/pic32Config.o ${OBJECTDIR}/src/UART/serial.o ${OBJECTDIR}/src/UART/serial_isr.o ${OBJECTDIR}/src/main.o
POSSIBLE_DEPFILES=${OBJECTDIR}/src/ADC/adc.o.d ${OBJECTDIR}/src/ADC/adc_isr.o.d ${OBJECTDIR}/src/COMMON/delay.o.d ${OBJECTDIR}/src/COMMON/func.o.d ${OBJECTDIR}/src/COMMON/timer.o.d ${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d ${OBJECTDIR}/src/COMMON/timer_isr.o.d ${OBJECTDIR}/src/GPS/gps.o.d ${OBJECTDIR}/src/GPS/gps_parser.o.d ${OBJECTDIR}/src/JPEG/ImageDecoder.o.d ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d ${OBJECTDIR}/src/JPEG/jidctint.o.d ${OBJECTDIR}/src/JPEG/jpegData.o.d ${OBJECTDIR}/src/JPEG/JpegDecoder.o.d ${OBJECTDIR}/src/JPEG/JPEGImage.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d ${OBJECTDIR}/src/LCD/displayMode.o.d ${OBJECTDIR}/src/LCD/lcd_touch.o.d ${OBJECTDIR}/src/PORT/port.o.d ${OBJECTDIR}/src/PORT/port_isr.o.d ${OBJECTDIR}/src/SD/sd.o.d ${OBJECTDIR}/src/SD/skSDlib.o.d ${OBJECTDIR}/src/SD/skSPIlib.o.d ${OBJECTDIR}/_ext/38565645/heap_1.o.d ${OBJECTDIR}/_ext/339404384/port.o.d ${OBJECTDIR}/_ext/339404384/port_asm.o.d ${OBJECTDIR}/_ext/1728301206/croutine.o.d ${OBJECTDIR}/_ext/1728301206/list.o.d ${OBJECTDIR}/_ext/1728301206/queue.o.d ${OBJECTDIR}/_ext/1728301206/tasks.o.d ${OBJECTDIR}/_ext/1728301206/event_groups.o.d ${OBJECTDIR}/_ext/1728301206/timers.o.d ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d ${OBJECTDIR}/src/SYSTEM/pic32Config.o.d ${OBJECTDIR}/src/UART/serial.o.d ${OBJECTDIR}/src/UART/serial_isr.o.d ${OBJECTDIR}/src/main.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/src/ADC/adc.o ${OBJECTDIR}/src/ADC/adc_isr.o ${OBJECTDIR}/src/COMMON/delay.o ${OBJECTDIR}/src/COMMON/func.o ${OBJECTDIR}/src/COMMON/timer.o ${OBJECTDIR}/src/COMMON/tiny_sprintf.o ${OBJECTDIR}/src/COMMON/timer_isr.o ${OBJECTDIR}/src/GPS/gps.o ${OBJECTDIR}/src/GPS/gps_parser.o ${OBJECTDIR}/src/JPEG/ImageDecoder.o ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o ${OBJECTDIR}/src/JPEG/jidctint.o ${OBJECTDIR}/src/JPEG/jpegData.o ${OBJECTDIR}/src/JPEG/JpegDecoder.o ${OBJECTDIR}/src/JPEG/JPEGImage.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o ${OBJECTDIR}/src/LCD/displayMode.o ${OBJECTDIR}/src/LCD/lcd_touch.o ${OBJECTDIR}/src/PORT/port.o ${OBJECTDIR}/src/PORT/port_isr.o ${OBJECTDIR}/src/SD/sd.o ${OBJECTDIR}/src/SD/skSDlib.o ${OBJECTDIR}/src/SD/skSPIlib.o ${OBJECTDIR}/_ext/38565645/heap_1.o ${OBJECTDIR}/_ext/339404384/port.o ${OBJECTDIR}/_ext/339404384/port_asm.o ${OBJECTDIR}/_ext/1728301206/croutine.o ${OBJECTDIR}/_ext/1728301206/list.o ${OBJECTDIR}/_ext/1728301206/queue.o ${OBJECTDIR}/_ext/1728301206/tasks.o ${OBJECTDIR}/_ext/1728301206/event_groups.o ${OBJECTDIR}/_ext/1728301206/timers.o ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o ${OBJECTDIR}/src/SYSTEM/pic32Config.o ${OBJECTDIR}/src/UART/serial.o ${OBJECTDIR}/src/UART/serial_isr.o ${OBJECTDIR}/src/main.o

# Source Files
SOURCEFILES=src/ADC/adc.c src/ADC/adc_isr.S src/COMMON/delay.c src/COMMON/func.c src/COMMON/timer.c src/COMMON/tiny_sprintf.c src/COMMON/timer_isr.S src/GPS/gps.c src/GPS/gps_parser.c src/JPEG/ImageDecoder.c src/JPEG/InternalFlashJPEGPicturesC32.c src/JPEG/jidctint.c src/JPEG/jpegData.c src/JPEG/JpegDecoder.c src/JPEG/JPEGImage.c src/LCD/LCD_DispMode/displayBody.c src/LCD/LCD_DispMode/displayMode_current.c src/LCD/LCD_DispMode/displayMode_draw.c src/LCD/LCD_DispMode/displayMode_gps.c src/LCD/LCD_DispMode/displayMode_graph.c src/LCD/LCD_DispMode/displayMode_nomal.c src/LCD/LCD_DispMode/displayMode_picture.c src/LCD/LCD_DispMode/displayMode_speed.c src/LCD/LCD_DispMode/displayMode_volt.c src/LCD/LCD_DispMode/displayMode_menu.c src/LCD/LCD_DispMode/displayTaskBar.c src/LCD/LCD_Lib/colorlcd_libdsPICVH.c src/LCD/LCD_Lib/digitalMeter.c src/LCD/LCD_Lib/displayControl.c src/LCD/LCD_Lib/displayGraph.c src/LCD/displayMode.c src/LCD/lcd_touch.c src/PORT/port.c src/PORT/port_isr.S src/SD/sd.c src/SD/skSDlib.c src/SD/skSPIlib.c ../Source/portable/MemMang/heap_1.c ../Source/portable/port.c ../Source/portable/port_asm.S ../Source/croutine.c ../Source/list.c ../Source/queue.c ../Source/tasks.c ../Source/event_groups.c ../Source/timers.c src/SYSTEM/ConfigPerformance.c src/SYSTEM/FreeRTOSHook.c src/SYSTEM/pic32Config.c src/UART/serial.c src/UART/serial_isr.S src/main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MX775F512H
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/src/ADC/adc_isr.o: src/ADC/adc_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/ADC" 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o.d 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o.ok ${OBJECTDIR}/src/ADC/adc_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/ADC/adc_isr.o.d" "${OBJECTDIR}/src/ADC/adc_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/ADC/adc_isr.o.d"  -o ${OBJECTDIR}/src/ADC/adc_isr.o src/ADC/adc_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/ADC/adc_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1
	
${OBJECTDIR}/src/COMMON/timer_isr.o: src/COMMON/timer_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o.ok ${OBJECTDIR}/src/COMMON/timer_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/timer_isr.o.d" "${OBJECTDIR}/src/COMMON/timer_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/COMMON/timer_isr.o.d"  -o ${OBJECTDIR}/src/COMMON/timer_isr.o src/COMMON/timer_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/COMMON/timer_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1
	
${OBJECTDIR}/src/PORT/port_isr.o: src/PORT/port_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/PORT" 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o.d 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o.ok ${OBJECTDIR}/src/PORT/port_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/PORT/port_isr.o.d" "${OBJECTDIR}/src/PORT/port_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/PORT/port_isr.o.d"  -o ${OBJECTDIR}/src/PORT/port_isr.o src/PORT/port_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/PORT/port_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1
	
${OBJECTDIR}/_ext/339404384/port_asm.o: ../Source/portable/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/339404384" 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o.ok ${OBJECTDIR}/_ext/339404384/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/339404384/port_asm.o.d" "${OBJECTDIR}/_ext/339404384/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/_ext/339404384/port_asm.o.d"  -o ${OBJECTDIR}/_ext/339404384/port_asm.o ../Source/portable/port_asm.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/339404384/port_asm.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1
	
${OBJECTDIR}/src/UART/serial_isr.o: src/UART/serial_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/UART" 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o.d 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o.ok ${OBJECTDIR}/src/UART/serial_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/UART/serial_isr.o.d" "${OBJECTDIR}/src/UART/serial_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/UART/serial_isr.o.d"  -o ${OBJECTDIR}/src/UART/serial_isr.o src/UART/serial_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/UART/serial_isr.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1
	
else
${OBJECTDIR}/src/ADC/adc_isr.o: src/ADC/adc_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/ADC" 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o.d 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o 
	@${RM} ${OBJECTDIR}/src/ADC/adc_isr.o.ok ${OBJECTDIR}/src/ADC/adc_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/ADC/adc_isr.o.d" "${OBJECTDIR}/src/ADC/adc_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/ADC/adc_isr.o.d"  -o ${OBJECTDIR}/src/ADC/adc_isr.o src/ADC/adc_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/ADC/adc_isr.o.asm.d",--gdwarf-2
	
${OBJECTDIR}/src/COMMON/timer_isr.o: src/COMMON/timer_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o 
	@${RM} ${OBJECTDIR}/src/COMMON/timer_isr.o.ok ${OBJECTDIR}/src/COMMON/timer_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/timer_isr.o.d" "${OBJECTDIR}/src/COMMON/timer_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/COMMON/timer_isr.o.d"  -o ${OBJECTDIR}/src/COMMON/timer_isr.o src/COMMON/timer_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/COMMON/timer_isr.o.asm.d",--gdwarf-2
	
${OBJECTDIR}/src/PORT/port_isr.o: src/PORT/port_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/PORT" 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o.d 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o 
	@${RM} ${OBJECTDIR}/src/PORT/port_isr.o.ok ${OBJECTDIR}/src/PORT/port_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/PORT/port_isr.o.d" "${OBJECTDIR}/src/PORT/port_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/PORT/port_isr.o.d"  -o ${OBJECTDIR}/src/PORT/port_isr.o src/PORT/port_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/PORT/port_isr.o.asm.d",--gdwarf-2
	
${OBJECTDIR}/_ext/339404384/port_asm.o: ../Source/portable/port_asm.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/339404384" 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o.d 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o 
	@${RM} ${OBJECTDIR}/_ext/339404384/port_asm.o.ok ${OBJECTDIR}/_ext/339404384/port_asm.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/339404384/port_asm.o.d" "${OBJECTDIR}/_ext/339404384/port_asm.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/_ext/339404384/port_asm.o.d"  -o ${OBJECTDIR}/_ext/339404384/port_asm.o ../Source/portable/port_asm.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/339404384/port_asm.o.asm.d",--gdwarf-2
	
${OBJECTDIR}/src/UART/serial_isr.o: src/UART/serial_isr.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/UART" 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o.d 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o 
	@${RM} ${OBJECTDIR}/src/UART/serial_isr.o.ok ${OBJECTDIR}/src/UART/serial_isr.o.err 
	@${FIXDEPS} "${OBJECTDIR}/src/UART/serial_isr.o.d" "${OBJECTDIR}/src/UART/serial_isr.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION) -I"src" -I"../Source/portable" -I"src/SYSTEM" -MMD -MF "${OBJECTDIR}/src/UART/serial_isr.o.d"  -o ${OBJECTDIR}/src/UART/serial_isr.o src/UART/serial_isr.S  -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/src/UART/serial_isr.o.asm.d",--gdwarf-2
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/src/ADC/adc.o: src/ADC/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/ADC" 
	@${RM} ${OBJECTDIR}/src/ADC/adc.o.d 
	@${RM} ${OBJECTDIR}/src/ADC/adc.o 
	@${FIXDEPS} "${OBJECTDIR}/src/ADC/adc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/ADC/adc.o.d" -o ${OBJECTDIR}/src/ADC/adc.o src/ADC/adc.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/delay.o: src/COMMON/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/delay.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/delay.o.d" -o ${OBJECTDIR}/src/COMMON/delay.o src/COMMON/delay.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/func.o: src/COMMON/func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/func.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/func.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/func.o.d" -o ${OBJECTDIR}/src/COMMON/func.o src/COMMON/func.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/timer.o: src/COMMON/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/timer.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/timer.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/timer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/timer.o.d" -o ${OBJECTDIR}/src/COMMON/timer.o src/COMMON/timer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/tiny_sprintf.o: src/COMMON/tiny_sprintf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/tiny_sprintf.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d" -o ${OBJECTDIR}/src/COMMON/tiny_sprintf.o src/COMMON/tiny_sprintf.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/GPS/gps.o: src/GPS/gps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/GPS" 
	@${RM} ${OBJECTDIR}/src/GPS/gps.o.d 
	@${RM} ${OBJECTDIR}/src/GPS/gps.o 
	@${FIXDEPS} "${OBJECTDIR}/src/GPS/gps.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/GPS/gps.o.d" -o ${OBJECTDIR}/src/GPS/gps.o src/GPS/gps.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/GPS/gps_parser.o: src/GPS/gps_parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/GPS" 
	@${RM} ${OBJECTDIR}/src/GPS/gps_parser.o.d 
	@${RM} ${OBJECTDIR}/src/GPS/gps_parser.o 
	@${FIXDEPS} "${OBJECTDIR}/src/GPS/gps_parser.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/GPS/gps_parser.o.d" -o ${OBJECTDIR}/src/GPS/gps_parser.o src/GPS/gps_parser.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/ImageDecoder.o: src/JPEG/ImageDecoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/ImageDecoder.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/ImageDecoder.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/ImageDecoder.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/ImageDecoder.o.d" -o ${OBJECTDIR}/src/JPEG/ImageDecoder.o src/JPEG/ImageDecoder.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o: src/JPEG/InternalFlashJPEGPicturesC32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d" -o ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o src/JPEG/InternalFlashJPEGPicturesC32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/jidctint.o: src/JPEG/jidctint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/jidctint.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/jidctint.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/jidctint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/jidctint.o.d" -o ${OBJECTDIR}/src/JPEG/jidctint.o src/JPEG/jidctint.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/jpegData.o: src/JPEG/jpegData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/jpegData.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/jpegData.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/jpegData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/jpegData.o.d" -o ${OBJECTDIR}/src/JPEG/jpegData.o src/JPEG/jpegData.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/JpegDecoder.o: src/JPEG/JpegDecoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/JpegDecoder.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/JpegDecoder.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/JpegDecoder.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/JpegDecoder.o.d" -o ${OBJECTDIR}/src/JPEG/JpegDecoder.o src/JPEG/JpegDecoder.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/JPEGImage.o: src/JPEG/JPEGImage.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/JPEGImage.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/JPEGImage.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/JPEGImage.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/JPEGImage.o.d" -o ${OBJECTDIR}/src/JPEG/JPEGImage.o src/JPEG/JPEGImage.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o: src/LCD/LCD_DispMode/displayBody.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o src/LCD/LCD_DispMode/displayBody.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o: src/LCD/LCD_DispMode/displayMode_current.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o src/LCD/LCD_DispMode/displayMode_current.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o: src/LCD/LCD_DispMode/displayMode_draw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o src/LCD/LCD_DispMode/displayMode_draw.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o: src/LCD/LCD_DispMode/displayMode_gps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o src/LCD/LCD_DispMode/displayMode_gps.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o: src/LCD/LCD_DispMode/displayMode_graph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o src/LCD/LCD_DispMode/displayMode_graph.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o: src/LCD/LCD_DispMode/displayMode_nomal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o src/LCD/LCD_DispMode/displayMode_nomal.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o: src/LCD/LCD_DispMode/displayMode_picture.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o src/LCD/LCD_DispMode/displayMode_picture.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o: src/LCD/LCD_DispMode/displayMode_speed.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o src/LCD/LCD_DispMode/displayMode_speed.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o: src/LCD/LCD_DispMode/displayMode_volt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o src/LCD/LCD_DispMode/displayMode_volt.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o: src/LCD/LCD_DispMode/displayMode_menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o src/LCD/LCD_DispMode/displayMode_menu.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o: src/LCD/LCD_DispMode/displayTaskBar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o src/LCD/LCD_DispMode/displayTaskBar.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o: src/LCD/LCD_Lib/colorlcd_libdsPICVH.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o src/LCD/LCD_Lib/colorlcd_libdsPICVH.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o: src/LCD/LCD_Lib/digitalMeter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o src/LCD/LCD_Lib/digitalMeter.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o: src/LCD/LCD_Lib/displayControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o src/LCD/LCD_Lib/displayControl.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o: src/LCD/LCD_Lib/displayGraph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o src/LCD/LCD_Lib/displayGraph.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/displayMode.o: src/LCD/displayMode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD" 
	@${RM} ${OBJECTDIR}/src/LCD/displayMode.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/displayMode.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/displayMode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/displayMode.o.d" -o ${OBJECTDIR}/src/LCD/displayMode.o src/LCD/displayMode.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/lcd_touch.o: src/LCD/lcd_touch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD" 
	@${RM} ${OBJECTDIR}/src/LCD/lcd_touch.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/lcd_touch.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/lcd_touch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/lcd_touch.o.d" -o ${OBJECTDIR}/src/LCD/lcd_touch.o src/LCD/lcd_touch.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/PORT/port.o: src/PORT/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/PORT" 
	@${RM} ${OBJECTDIR}/src/PORT/port.o.d 
	@${RM} ${OBJECTDIR}/src/PORT/port.o 
	@${FIXDEPS} "${OBJECTDIR}/src/PORT/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/PORT/port.o.d" -o ${OBJECTDIR}/src/PORT/port.o src/PORT/port.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/sd.o: src/SD/sd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/sd.o.d 
	@${RM} ${OBJECTDIR}/src/SD/sd.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/sd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/sd.o.d" -o ${OBJECTDIR}/src/SD/sd.o src/SD/sd.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/skSDlib.o: src/SD/skSDlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/skSDlib.o.d 
	@${RM} ${OBJECTDIR}/src/SD/skSDlib.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/skSDlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/skSDlib.o.d" -o ${OBJECTDIR}/src/SD/skSDlib.o src/SD/skSDlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/skSPIlib.o: src/SD/skSPIlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/skSPIlib.o.d 
	@${RM} ${OBJECTDIR}/src/SD/skSPIlib.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/skSPIlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/skSPIlib.o.d" -o ${OBJECTDIR}/src/SD/skSPIlib.o src/SD/skSPIlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/38565645/heap_1.o: ../Source/portable/MemMang/heap_1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/38565645" 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/38565645/heap_1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/38565645/heap_1.o.d" -o ${OBJECTDIR}/_ext/38565645/heap_1.o ../Source/portable/MemMang/heap_1.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/339404384/port.o: ../Source/portable/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/339404384" 
	@${RM} ${OBJECTDIR}/_ext/339404384/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/339404384/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/339404384/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/339404384/port.o.d" -o ${OBJECTDIR}/_ext/339404384/port.o ../Source/portable/port.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/croutine.o: ../Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/croutine.o.d" -o ${OBJECTDIR}/_ext/1728301206/croutine.o ../Source/croutine.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/list.o: ../Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/list.o.d" -o ${OBJECTDIR}/_ext/1728301206/list.o ../Source/list.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/queue.o: ../Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/queue.o.d" -o ${OBJECTDIR}/_ext/1728301206/queue.o ../Source/queue.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/tasks.o: ../Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/tasks.o.d" -o ${OBJECTDIR}/_ext/1728301206/tasks.o ../Source/tasks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/event_groups.o: ../Source/event_groups.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/event_groups.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/event_groups.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/event_groups.o.d" -o ${OBJECTDIR}/_ext/1728301206/event_groups.o ../Source/event_groups.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/timers.o: ../Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/timers.o.d" -o ${OBJECTDIR}/_ext/1728301206/timers.o ../Source/timers.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o: src/SYSTEM/ConfigPerformance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d" -o ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o src/SYSTEM/ConfigPerformance.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o: src/SYSTEM/FreeRTOSHook.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d" -o ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o src/SYSTEM/FreeRTOSHook.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/pic32Config.o: src/SYSTEM/pic32Config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/pic32Config.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/pic32Config.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/pic32Config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/pic32Config.o.d" -o ${OBJECTDIR}/src/SYSTEM/pic32Config.o src/SYSTEM/pic32Config.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/UART/serial.o: src/UART/serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/UART" 
	@${RM} ${OBJECTDIR}/src/UART/serial.o.d 
	@${RM} ${OBJECTDIR}/src/UART/serial.o 
	@${FIXDEPS} "${OBJECTDIR}/src/UART/serial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/UART/serial.o.d" -o ${OBJECTDIR}/src/UART/serial.o src/UART/serial.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/main.o: src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/main.o.d 
	@${RM} ${OBJECTDIR}/src/main.o 
	@${FIXDEPS} "${OBJECTDIR}/src/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1 -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/main.o.d" -o ${OBJECTDIR}/src/main.o src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/src/ADC/adc.o: src/ADC/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/ADC" 
	@${RM} ${OBJECTDIR}/src/ADC/adc.o.d 
	@${RM} ${OBJECTDIR}/src/ADC/adc.o 
	@${FIXDEPS} "${OBJECTDIR}/src/ADC/adc.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/ADC/adc.o.d" -o ${OBJECTDIR}/src/ADC/adc.o src/ADC/adc.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/delay.o: src/COMMON/delay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/delay.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/delay.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/delay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/delay.o.d" -o ${OBJECTDIR}/src/COMMON/delay.o src/COMMON/delay.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/func.o: src/COMMON/func.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/func.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/func.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/func.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/func.o.d" -o ${OBJECTDIR}/src/COMMON/func.o src/COMMON/func.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/timer.o: src/COMMON/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/timer.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/timer.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/timer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/timer.o.d" -o ${OBJECTDIR}/src/COMMON/timer.o src/COMMON/timer.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/COMMON/tiny_sprintf.o: src/COMMON/tiny_sprintf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/COMMON" 
	@${RM} ${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d 
	@${RM} ${OBJECTDIR}/src/COMMON/tiny_sprintf.o 
	@${FIXDEPS} "${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/COMMON/tiny_sprintf.o.d" -o ${OBJECTDIR}/src/COMMON/tiny_sprintf.o src/COMMON/tiny_sprintf.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/GPS/gps.o: src/GPS/gps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/GPS" 
	@${RM} ${OBJECTDIR}/src/GPS/gps.o.d 
	@${RM} ${OBJECTDIR}/src/GPS/gps.o 
	@${FIXDEPS} "${OBJECTDIR}/src/GPS/gps.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/GPS/gps.o.d" -o ${OBJECTDIR}/src/GPS/gps.o src/GPS/gps.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/GPS/gps_parser.o: src/GPS/gps_parser.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/GPS" 
	@${RM} ${OBJECTDIR}/src/GPS/gps_parser.o.d 
	@${RM} ${OBJECTDIR}/src/GPS/gps_parser.o 
	@${FIXDEPS} "${OBJECTDIR}/src/GPS/gps_parser.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/GPS/gps_parser.o.d" -o ${OBJECTDIR}/src/GPS/gps_parser.o src/GPS/gps_parser.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/ImageDecoder.o: src/JPEG/ImageDecoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/ImageDecoder.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/ImageDecoder.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/ImageDecoder.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/ImageDecoder.o.d" -o ${OBJECTDIR}/src/JPEG/ImageDecoder.o src/JPEG/ImageDecoder.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o: src/JPEG/InternalFlashJPEGPicturesC32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o.d" -o ${OBJECTDIR}/src/JPEG/InternalFlashJPEGPicturesC32.o src/JPEG/InternalFlashJPEGPicturesC32.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/jidctint.o: src/JPEG/jidctint.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/jidctint.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/jidctint.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/jidctint.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/jidctint.o.d" -o ${OBJECTDIR}/src/JPEG/jidctint.o src/JPEG/jidctint.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/jpegData.o: src/JPEG/jpegData.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/jpegData.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/jpegData.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/jpegData.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/jpegData.o.d" -o ${OBJECTDIR}/src/JPEG/jpegData.o src/JPEG/jpegData.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/JpegDecoder.o: src/JPEG/JpegDecoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/JpegDecoder.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/JpegDecoder.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/JpegDecoder.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/JpegDecoder.o.d" -o ${OBJECTDIR}/src/JPEG/JpegDecoder.o src/JPEG/JpegDecoder.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/JPEG/JPEGImage.o: src/JPEG/JPEGImage.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/JPEG" 
	@${RM} ${OBJECTDIR}/src/JPEG/JPEGImage.o.d 
	@${RM} ${OBJECTDIR}/src/JPEG/JPEGImage.o 
	@${FIXDEPS} "${OBJECTDIR}/src/JPEG/JPEGImage.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/JPEG/JPEGImage.o.d" -o ${OBJECTDIR}/src/JPEG/JPEGImage.o src/JPEG/JPEGImage.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o: src/LCD/LCD_DispMode/displayBody.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayBody.o src/LCD/LCD_DispMode/displayBody.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o: src/LCD/LCD_DispMode/displayMode_current.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_current.o src/LCD/LCD_DispMode/displayMode_current.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o: src/LCD/LCD_DispMode/displayMode_draw.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_draw.o src/LCD/LCD_DispMode/displayMode_draw.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o: src/LCD/LCD_DispMode/displayMode_gps.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_gps.o src/LCD/LCD_DispMode/displayMode_gps.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o: src/LCD/LCD_DispMode/displayMode_graph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_graph.o src/LCD/LCD_DispMode/displayMode_graph.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o: src/LCD/LCD_DispMode/displayMode_nomal.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_nomal.o src/LCD/LCD_DispMode/displayMode_nomal.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o: src/LCD/LCD_DispMode/displayMode_picture.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_picture.o src/LCD/LCD_DispMode/displayMode_picture.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o: src/LCD/LCD_DispMode/displayMode_speed.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_speed.o src/LCD/LCD_DispMode/displayMode_speed.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o: src/LCD/LCD_DispMode/displayMode_volt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_volt.o src/LCD/LCD_DispMode/displayMode_volt.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o: src/LCD/LCD_DispMode/displayMode_menu.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayMode_menu.o src/LCD/LCD_DispMode/displayMode_menu.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o: src/LCD/LCD_DispMode/displayTaskBar.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_DispMode" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o.d" -o ${OBJECTDIR}/src/LCD/LCD_DispMode/displayTaskBar.o src/LCD/LCD_DispMode/displayTaskBar.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o: src/LCD/LCD_Lib/colorlcd_libdsPICVH.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/colorlcd_libdsPICVH.o src/LCD/LCD_Lib/colorlcd_libdsPICVH.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o: src/LCD/LCD_Lib/digitalMeter.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/digitalMeter.o src/LCD/LCD_Lib/digitalMeter.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o: src/LCD/LCD_Lib/displayControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/displayControl.o src/LCD/LCD_Lib/displayControl.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o: src/LCD/LCD_Lib/displayGraph.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD/LCD_Lib" 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o.d" -o ${OBJECTDIR}/src/LCD/LCD_Lib/displayGraph.o src/LCD/LCD_Lib/displayGraph.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/displayMode.o: src/LCD/displayMode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD" 
	@${RM} ${OBJECTDIR}/src/LCD/displayMode.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/displayMode.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/displayMode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/displayMode.o.d" -o ${OBJECTDIR}/src/LCD/displayMode.o src/LCD/displayMode.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/LCD/lcd_touch.o: src/LCD/lcd_touch.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/LCD" 
	@${RM} ${OBJECTDIR}/src/LCD/lcd_touch.o.d 
	@${RM} ${OBJECTDIR}/src/LCD/lcd_touch.o 
	@${FIXDEPS} "${OBJECTDIR}/src/LCD/lcd_touch.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/LCD/lcd_touch.o.d" -o ${OBJECTDIR}/src/LCD/lcd_touch.o src/LCD/lcd_touch.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/PORT/port.o: src/PORT/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/PORT" 
	@${RM} ${OBJECTDIR}/src/PORT/port.o.d 
	@${RM} ${OBJECTDIR}/src/PORT/port.o 
	@${FIXDEPS} "${OBJECTDIR}/src/PORT/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/PORT/port.o.d" -o ${OBJECTDIR}/src/PORT/port.o src/PORT/port.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/sd.o: src/SD/sd.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/sd.o.d 
	@${RM} ${OBJECTDIR}/src/SD/sd.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/sd.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/sd.o.d" -o ${OBJECTDIR}/src/SD/sd.o src/SD/sd.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/skSDlib.o: src/SD/skSDlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/skSDlib.o.d 
	@${RM} ${OBJECTDIR}/src/SD/skSDlib.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/skSDlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/skSDlib.o.d" -o ${OBJECTDIR}/src/SD/skSDlib.o src/SD/skSDlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SD/skSPIlib.o: src/SD/skSPIlib.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SD" 
	@${RM} ${OBJECTDIR}/src/SD/skSPIlib.o.d 
	@${RM} ${OBJECTDIR}/src/SD/skSPIlib.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SD/skSPIlib.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SD/skSPIlib.o.d" -o ${OBJECTDIR}/src/SD/skSPIlib.o src/SD/skSPIlib.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/38565645/heap_1.o: ../Source/portable/MemMang/heap_1.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/38565645" 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o.d 
	@${RM} ${OBJECTDIR}/_ext/38565645/heap_1.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/38565645/heap_1.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/38565645/heap_1.o.d" -o ${OBJECTDIR}/_ext/38565645/heap_1.o ../Source/portable/MemMang/heap_1.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/339404384/port.o: ../Source/portable/port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/339404384" 
	@${RM} ${OBJECTDIR}/_ext/339404384/port.o.d 
	@${RM} ${OBJECTDIR}/_ext/339404384/port.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/339404384/port.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/339404384/port.o.d" -o ${OBJECTDIR}/_ext/339404384/port.o ../Source/portable/port.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/croutine.o: ../Source/croutine.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/croutine.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/croutine.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/croutine.o.d" -o ${OBJECTDIR}/_ext/1728301206/croutine.o ../Source/croutine.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/list.o: ../Source/list.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/list.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/list.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/list.o.d" -o ${OBJECTDIR}/_ext/1728301206/list.o ../Source/list.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/queue.o: ../Source/queue.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/queue.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/queue.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/queue.o.d" -o ${OBJECTDIR}/_ext/1728301206/queue.o ../Source/queue.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/tasks.o: ../Source/tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/tasks.o.d" -o ${OBJECTDIR}/_ext/1728301206/tasks.o ../Source/tasks.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/event_groups.o: ../Source/event_groups.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/event_groups.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/event_groups.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/event_groups.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/event_groups.o.d" -o ${OBJECTDIR}/_ext/1728301206/event_groups.o ../Source/event_groups.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1728301206/timers.o: ../Source/timers.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1728301206" 
	@${RM} ${OBJECTDIR}/_ext/1728301206/timers.o.d 
	@${RM} ${OBJECTDIR}/_ext/1728301206/timers.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1728301206/timers.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/_ext/1728301206/timers.o.d" -o ${OBJECTDIR}/_ext/1728301206/timers.o ../Source/timers.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o: src/SYSTEM/ConfigPerformance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o.d" -o ${OBJECTDIR}/src/SYSTEM/ConfigPerformance.o src/SYSTEM/ConfigPerformance.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o: src/SYSTEM/FreeRTOSHook.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o.d" -o ${OBJECTDIR}/src/SYSTEM/FreeRTOSHook.o src/SYSTEM/FreeRTOSHook.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/SYSTEM/pic32Config.o: src/SYSTEM/pic32Config.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/SYSTEM" 
	@${RM} ${OBJECTDIR}/src/SYSTEM/pic32Config.o.d 
	@${RM} ${OBJECTDIR}/src/SYSTEM/pic32Config.o 
	@${FIXDEPS} "${OBJECTDIR}/src/SYSTEM/pic32Config.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/SYSTEM/pic32Config.o.d" -o ${OBJECTDIR}/src/SYSTEM/pic32Config.o src/SYSTEM/pic32Config.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/UART/serial.o: src/UART/serial.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/UART" 
	@${RM} ${OBJECTDIR}/src/UART/serial.o.d 
	@${RM} ${OBJECTDIR}/src/UART/serial.o 
	@${FIXDEPS} "${OBJECTDIR}/src/UART/serial.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/UART/serial.o.d" -o ${OBJECTDIR}/src/UART/serial.o src/UART/serial.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/src/main.o: src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/main.o.d 
	@${RM} ${OBJECTDIR}/src/main.o 
	@${FIXDEPS} "${OBJECTDIR}/src/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -I"../Source" -I"../Source/include" -I"../Source/portable" -I"src" -I"src/ADC" -I"src/COMMON" -I"src/GPS" -I"src/JPEG" -I"src/LCD" -I"src/SD" -I"src/UART" -I"src/SYSTEM" -I"src/LCD/LCD_DispMode" -I"src/LCD/LCD_Lib" -I"src/PORT" -MMD -MF "${OBJECTDIR}/src/main.o.d" -o ${OBJECTDIR}/src/main.o src/main.c    -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mdebugger -D__MPLAB_DEBUGGER_PK3=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)    -mreserve=boot@0x1FC02000:0x1FC02FEF -mreserve=boot@0x1FC02000:0x1FC024FF  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,--defsym=_min_heap_size=100,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}          -DXPRJ_default=$(CND_CONF)  -no-legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=100,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/BikeMeter.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
