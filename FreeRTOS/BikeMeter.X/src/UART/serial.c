/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     serial.c
 *
 *     シリアルデバッグ出力 GPS入力
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <xc.h>
#include "serial.h"
#include "FreeRTOS.h"
#include "semphr.h"


/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define UART_DBG_SND_BUF_SIZE			(0x400)
#define UART_DBG_RCV_BUF_SIZE			(0x40)
#define UART_GPS_SND_BUF_SIZE			(0x100)
#define UART_GPS_RCV_BUF_SIZE			(0x100)

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/*
 *  デバッグ送信バッファ
 */
typedef struct _UART_DBG_SND_BUF {
    unsigned short wp;
    unsigned short rp;
    char dat[UART_DBG_SND_BUF_SIZE];
} UART_DBG_SND_BUF;

/*
 *  デバッグ受信バッファ
 */
typedef struct _UART_DBG_RCV_BUF {
    unsigned short wp;
    unsigned short rp;
    char dat[UART_DBG_RCV_BUF_SIZE];
} UART_DBG_RCV_BUF;

/*
 *  GPS送信バッファ
 */
typedef struct _UART_GPS_SND_BUF {
    unsigned short wp;
    unsigned short rp;
    char dat[UART_GPS_SND_BUF_SIZE];
} UART_GPS_SND_BUF;

/*
 *  GPS受信バッファ
 */
typedef struct _UART_GPS_RCV_BUF {
    unsigned short wp;
    unsigned short rp;
    char dat[UART_GPS_RCV_BUF_SIZE];
} UART_GPS_RCV_BUF;

/* --- 変数宣言 --------------------------------------------------------------*/
static UART_DBG_SND_BUF uart_dbg_snd_buf; // デバッグ出力用バッファ
static UART_DBG_RCV_BUF uart_dbg_rcv_buf; // デバッグ入力用バッファ
static UART_GPS_SND_BUF uart_gps_snd_buf; // GPS出力用バッファ
static UART_GPS_RCV_BUF uart_gps_rcv_buf; // GPS入力用バッファ

static SemaphoreHandle_t uart_dbg_snd_semaphore;
static SemaphoreHandle_t uart_gps_snd_semaphore;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
void __attribute__((interrupt(IPL0AUTO), vector(_UART_3_VECTOR))) vU3InterruptWrapper(void);
void __attribute__((interrupt(IPL0AUTO), vector(_UART_6_VECTOR))) vU6InterruptWrapper(void);

/* --- ローカル関数群 --------------------------------------------------------*/

/*
 *  デバッグ出力 送信バッファ 1文字格納
 */
void uart_debug_snd_buf_putc(char dat) {
    unsigned short wp_old = 0;

    uart_dbg_snd_buf.dat[uart_dbg_snd_buf.wp] = dat;
    wp_old = uart_dbg_snd_buf.wp;
    uart_dbg_snd_buf.wp++;
    if (UART_DBG_SND_BUF_SIZE <= uart_dbg_snd_buf.wp) {
        uart_dbg_snd_buf.wp = 0;
    }

    return;
} // uart_debug_snd_buf_putc

/*
 *  GPS出力 送信バッファ 1文字格納
 */
void uart_gps_snd_buf_putc(char dat) {
    unsigned short wp_old = 0;

    uart_gps_snd_buf.dat[uart_gps_snd_buf.wp] = dat;
    wp_old = uart_gps_snd_buf.wp;
    uart_gps_snd_buf.wp++;
    if (UART_GPS_SND_BUF_SIZE <= uart_gps_snd_buf.wp) {
        uart_gps_snd_buf.wp = 0;
    }

    return;
} // uart_gps_snd_buf_putc

/* --- グローバル関数群 ------------------------------------------------------*/

/*
 *  uart 初期化
 */
void uart_initialize(void) {

    // バッファ初期化
    uart_dbg_snd_buf.wp = 0;
    uart_dbg_snd_buf.rp = 0;
    memset(uart_dbg_snd_buf.dat, 0, UART_DBG_SND_BUF_SIZE);
    uart_dbg_rcv_buf.wp = 0;
    uart_dbg_rcv_buf.rp = 0;
    memset(uart_dbg_rcv_buf.dat, 0, UART_DBG_RCV_BUF_SIZE);
    uart_gps_snd_buf.wp = 0;
    uart_gps_snd_buf.rp = 0;
    memset(uart_gps_snd_buf.dat, 0, UART_GPS_SND_BUF_SIZE);
    uart_gps_rcv_buf.wp = 0;
    uart_gps_rcv_buf.rp = 0;
    memset(uart_gps_rcv_buf.dat, 0, UART_GPS_RCV_BUF_SIZE);

    uart_dbg_snd_semaphore = xSemaphoreCreateBinary();
    if (uart_dbg_snd_semaphore == NULL) {
        // セマフォ初期化失敗
        return;
    }
    xSemaphoreGive(uart_dbg_snd_semaphore);

    uart_gps_snd_semaphore = xSemaphoreCreateBinary();
    if (uart_gps_snd_semaphore == NULL) {
        // セマフォ初期化失敗
        return;
    }
    xSemaphoreGive(uart_gps_snd_semaphore);

    // UART3 GPS
    U3BRG = 0x103; // 40MHz 9600bps
    U3STA = 0x00009400;
    U3MODE = 0x8000;

    // UART6 デバッグ
    U6BRG = 0x15; // 40MHz 115200bps
    U6STA = 0x00009400;
    U6MODE = 0x8000;

    // UART3
    IPC7bits.U3IP = 4;
    IPC7bits.U3IP = 4;
    IEC1bits.U3RXIE = 1;
    IEC1bits.U3TXIE = 0;
    IEC1bits.U3EIE = 1;

    // UART6
    IPC12bits.U6IP = 2;
    IPC12bits.U6IP = 2;
    IEC2bits.U6RXIE = 1;
    IEC2bits.U6TXIE = 0;
    IEC2bits.U6EIE = 1;

    return;
} // uart_initialize

/*
 *  デバッグ出力 送信バッファ 複数文字格納
 */
void uart_debug_snd_buf_puts(char *dat) {

    // 複数タスクでデバッグ送信をすることを考慮してセマフォでブロックする
    xSemaphoreTake(uart_dbg_snd_semaphore, portMAX_DELAY);

    unsigned short idx = 0;
    unsigned char send_flg = 0;

    if (uart_dbg_rcv_buf.wp == uart_dbg_rcv_buf.rp) {
        send_flg = 1;
    }

    for (idx = 0; dat[idx] != '\0'; ++idx) {
        uart_debug_snd_buf_putc(dat[idx]);
    }

    if (send_flg == 1) {
        U6TXREG = uart_dbg_snd_buf.dat[uart_dbg_snd_buf.rp];
        uart_dbg_snd_buf.rp++;
        if (UART_DBG_SND_BUF_SIZE <= uart_dbg_snd_buf.rp) {
            uart_dbg_snd_buf.rp = 0;
        }
        IEC2bits.U6TXIE = 1;
    }

    xSemaphoreGive(uart_dbg_snd_semaphore);

    return;
} // uart_debug_snd_buf_puts

/*
 *  デバッグ出力 送信バッファ 複数文字格納(割り込みなし)
 */
void uart_debug_snd_no_intr(char *dat) {
    int idx;
    for (idx = 0; dat[idx] != '\0'; ++idx) {
        while (U6STAbits.UTXBF == 1);
        U6TXREG = dat[idx];
    }
}

/*
 *  デバッグ入力 受信バッファ 1文字取得
 */
char uart_debug_rcv_buf_getc(void) {
    char dat = 0;

    dat = uart_dbg_rcv_buf.dat[uart_dbg_rcv_buf.rp];
    uart_dbg_rcv_buf.rp++;

    if (UART_DBG_RCV_BUF_SIZE <= uart_dbg_rcv_buf.rp) {
        uart_dbg_rcv_buf.rp = 0;
    }

    return dat;
} // uart_debug_rcv_buf_getc

/*
 *  デバッグ入力 受信バッファ あり・なし確認
 */
unsigned char uart_debug_rcv_buf_empty(void) {
    if (uart_dbg_rcv_buf.wp == uart_dbg_rcv_buf.rp) {
        return 1;
    } else {
        return 0;
    }
} // uart_debug_rcv_buf_get_empty

/*
 *  GPS出力 送信バッファ 複数文字格納
 */
void uart_gps_snd_buf_puts(char *dat, unsigned short len) {

    // 複数タスクでGPS送信をすることを考慮してセマフォでブロックする
    xSemaphoreTake(uart_gps_snd_semaphore, portMAX_DELAY);

    unsigned short idx = 0;
    unsigned char send_flg = 0;

    if (uart_gps_rcv_buf.wp == uart_gps_rcv_buf.rp) {
        send_flg = 1;
    }

    for (idx = 0; idx < len; idx++) {
        uart_gps_snd_buf_putc(dat[idx]);
    }

    if (send_flg == 1) {
        U3TXREG = uart_gps_snd_buf.dat[uart_gps_snd_buf.rp];
        uart_gps_snd_buf.rp++;
        if (UART_GPS_SND_BUF_SIZE <= uart_gps_snd_buf.rp) {
            uart_gps_snd_buf.rp = 0;
        }
        IEC1bits.U3TXIE = 1;
    }

    xSemaphoreGive(uart_gps_snd_semaphore);

    return;
} // uart_gps_snd_buf_puts

/*
 *  GPS入力 受信バッファ 1文字取得
 */
char uart_gps_rcv_buf_getc(void) {
    char dat = 0;

    dat = uart_gps_rcv_buf.dat[uart_gps_rcv_buf.rp];
    uart_gps_rcv_buf.rp++;

    if (UART_GPS_RCV_BUF_SIZE <= uart_gps_rcv_buf.rp) {
        uart_gps_rcv_buf.rp = 0;
    }
    
#if 0
    char test[2];
    sprintf(test, "%c", dat);
    uart_debug_snd_buf_puts(test);
#endif

    return dat;
} // uart_gps_rcv_buf_getc

/*
 *  GPS入力 受信バッファ あり・なし確認
 */
unsigned char uart_gps_rcv_buf_empty(void) {
    if (uart_gps_rcv_buf.wp == uart_gps_rcv_buf.rp) {
        return 1;
    } else {
        return 0;
    }
} // uart_gps_rcv_buf_get_empty

/*************************************
 *
 *   UART6 DEBUG 割込みサービスルーチン
 *
 *************************************/
void vU6InterruptHandler(void) {
    // デバッグシリアル受信
    if (IFS2bits.U6RXIF == 1) {
        while (U6STAbits.URXDA == 1) {
            uart_dbg_rcv_buf.dat[uart_dbg_rcv_buf.wp] = U6RXREG;
            uart_dbg_rcv_buf.wp++;
            if (UART_DBG_RCV_BUF_SIZE <= uart_dbg_rcv_buf.wp) {
                uart_dbg_rcv_buf.wp = 0;
            }
        }

        IFS2bits.U6RXIF = 0;
    }

    // デバッグシリアル送信
    if (IFS2bits.U6TXIF == 1) {
        IFS2bits.U6TXIF = 0;

        if (U6STAbits.TRMT == 1) {
            if (uart_dbg_snd_buf.wp != uart_dbg_snd_buf.rp) {
                U6TXREG = uart_dbg_snd_buf.dat[uart_dbg_snd_buf.rp];
                uart_dbg_snd_buf.rp++;
                if (UART_DBG_SND_BUF_SIZE <= uart_dbg_snd_buf.rp) {
                    uart_dbg_snd_buf.rp = 0;
                }
            } else {
                // 送信完了で割り込み無効
                IEC2bits.U6TXIE = 0;
            }
        }
    }

    // エラー検出
    if (IFS2bits.U6EIF == 1) {
        IFS2bits.U6EIF = 0;

        // バッファフローリセット
        U6STAbits.OERR = 0;
    }
}

/*************************************
 *
 *   UART3 GPS 割込みサービスルーチン
 *
 *************************************/
void vU3InterruptHandler(void) {
    // GPSシリアル受信
    if (IFS1bits.U3RXIF == 1) {
        while (U3STAbits.URXDA == 1) {
            uart_gps_rcv_buf.dat[uart_gps_rcv_buf.wp] = U3RXREG;
            uart_gps_rcv_buf.wp++;
            if (UART_GPS_RCV_BUF_SIZE <= uart_gps_rcv_buf.wp) {
                uart_gps_rcv_buf.wp = 0;
            }
        }

        IFS1bits.U3RXIF = 0;
    }

    // GPSシリアル送信
    if (IFS1bits.U3TXIF == 1) {
        IFS1bits.U3TXIF = 0;

        if (U3STAbits.TRMT == 1) {
            if (uart_gps_snd_buf.wp != uart_gps_snd_buf.rp) {
                U3TXREG = uart_gps_snd_buf.dat[uart_gps_snd_buf.rp];
                uart_gps_snd_buf.rp++;
                if (UART_GPS_SND_BUF_SIZE <= uart_gps_snd_buf.rp) {
                    uart_gps_snd_buf.rp = 0;
                }
            } else {
                // 送信完了で割り込み無効
                IEC1bits.U3TXIE = 0;
            }
        }
    }

    // エラー検出
    if (IFS1bits.U3EIF == 1) {
        IFS1bits.U3EIF = 0;

        // バッファフローリセット
        U3STAbits.OERR = 0;
    }
}

