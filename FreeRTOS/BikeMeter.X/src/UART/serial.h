/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     serial.h
 *
 *     シリアルデバッグ出力 GPS入力 ヘッダ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef __SERIAL_H_
#define __SERIAL_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void uart_initialize(void);
extern void uart_debug_snd_buf_puts(char *dat);
extern void uart_debug_snd_no_intr(char *dat);
extern char uart_debug_rcv_buf_getc(void);
extern unsigned char uart_debug_rcv_buf_empty(void);
extern void uart_gps_snd_buf_puts(char *dat, unsigned short len);
extern char uart_gps_rcv_buf_getc(void);
extern unsigned char uart_gps_rcv_buf_empty(void);


#endif /* __SERIAL_H_ */
