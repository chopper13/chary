/*
 * File:   sd.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __SD_H_
#define __SD_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/
enum eSdState {
    SD_STATE_STARTING = 0,
    SD_STATE_OPEN,
    SD_STATE_CLOSING,
    SD_STATE_CLOSE,
    SD_STATE_DETECT,
    SD_STATE_REINIT,
    SD_STATE_ERR,
    SD_STATE_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/
extern const char *sd_logging_str_list[SD_STATE_MAX];

/* --- 関数宣言 --------------------------------------------------------------*/
extern void sdCommInit(void);
extern void create_sd_control_task(void);
extern unsigned char get_sd_logging_state(void);

#endif	// __SD_H_

