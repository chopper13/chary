/*
 * File:   sd.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include"sd.h"
#include "FreeRTOS.h"
#include "task.h"

#include "skSPIlib.h"
#include "skSDlib.h"
#include "timer.h"
#include "serial.h"

#include "gps.h"
#include "adc.h"
#include "tiny_sprintf.h"
#include "func.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define SD_CD_TRIS	TRISDbits.TRISD8
#define SD_CD		PORTDbits.RD8

#define SD_LOG_WRITE_INTERVAL_MS 1000
#define SD_RE_INIT_INTERVAL_MS 1000

#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		1		// タスク優先度
#define TASK_STACK_SIZE		192		// スタックサイズ

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static unsigned char sd_mode_change_flg;
static unsigned char sd_logging_state;
static unsigned char write_count;


/* --- テーブル定義 ----------------------------------------------------------*/
const char *sd_logging_str_list[SD_STATE_MAX] = {
    " open",
    "write",
    "close",
    "close",
    "  det"
    "  err",
    "  err",
};
/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static void sdControl(void) {
    static TickType_t log_write_time_old;
    static TickType_t init_time_old;
    int ret;
    int i;

    // SDカード検出
    if (SD_CD == 0 && (sd_logging_state == SD_STATE_DETECT || (sd_logging_state == SD_STATE_REINIT) && (SD_RE_INIT_INTERVAL_MS < (xTaskGetTickCount() - log_write_time_old)))) {
        // SDカード初期化
        ret = SD_Init();
        log_write_time_old = time_delta_counter;


        if (ret == 0) {
            uart_debug_snd_buf_puts("---sd init success---\n");
            sd_logging_state = SD_STATE_CLOSE;
        } else if (ret == 0x00004000) {
            uart_debug_snd_buf_puts("---sd detect?---\n");
            sd_logging_state = SD_STATE_REINIT;
        } else {
            uart_debug_snd_buf_puts("---sd init error---\n");
            sd_logging_state = SD_STATE_ERR;
        }

        sd_mode_change_flg = 0;
    } else if (SD_CD == 1) {
        sd_logging_state = SD_STATE_DETECT;
    }

    // 書き込み開始、終了制御
    if (sd_mode_change_flg == 1) {
        sd_mode_change_flg = 0;

        if (sd_logging_state == SD_STATE_CLOSE) {
            // ファイルオープン
#if 1
            Str_Tsprintf(_str, "%02d%02d%02d%02d.TXT",
            get_gps_mon(), get_gps_day(),
            get_gps_hour(), get_gps_min());
#else
            Str_Tsprintf(_str, "%02d%02d%02d.TXT",
            get_gps_day(),
            get_gps_hour(), get_gps_min());
#endif
            ret = SD_Open(_str, O_RDWR);
            if (ret != 0) {
                uart_debug_snd_buf_puts("---sd open error---\n");
                sd_logging_state = SD_STATE_ERR;
            } else {
                uart_debug_snd_buf_puts("---sd open success---\n");
                sd_logging_state = SD_STATE_OPEN;
            }

            log_write_time_old = time_delta_counter;
        } else if (sd_logging_state == SD_STATE_OPEN) {
            // ファイルクローズ
            SD_Close();
            uart_debug_snd_buf_puts("---sd close success---\n");
            sd_logging_state = SD_STATE_CLOSE;
        }
    }

    if (sd_logging_state == SD_STATE_OPEN) {
        if (SD_LOG_WRITE_INTERVAL_MS < (xTaskGetTickCount() - log_write_time_old)) {
            log_write_time_old = time_delta_counter;

            int intValCourse = (int) get_gps_declination();
            int intValSpeed = (int) get_gps_speed();
            float tmpVal[AD_DATA_MAX];
            int intVal[AD_DATA_MAX];
            for (i = 0; i < AD_DATA_MAX; i++) {
                // AD値変換
                tmpVal[i] = get_actual_val(i);
                intVal[i] = (int) tmpVal[i];
            }

            // 時分秒,経度,緯度,進行方向,速度,回転数,油温,電圧,温度,電流1,電流2,電流3
            int size = Str_Tsprintf(_str, "%02d%02d%02d,%d.%d,%d.%d,%d.%d,%d.%d,%d,%d.%d,%d.%d,%d.%d,%d.%d,%d.%d,%d.%d\n",
            	get_gps_hour(), get_gps_min(), get_gps_sec(),
            	get_gps_lat_degree(), get_gps_lat_min(),
            	get_gps_lon_degree(), get_gps_lon_min(),
                    intValCourse, abs((int) ((get_gps_declination() - intValCourse) * 10)),
                    intValSpeed, abs((int) ((get_gps_speed() - intValSpeed) * 10)),
                    get_rpm(),
                    intVal[AD_DATA_OILTEMP], abs((int) ((tmpVal[AD_DATA_OILTEMP] - intVal[AD_DATA_OILTEMP]) * 10)),
                    intVal[AD_DATA_VOLT], abs((int) ((tmpVal[AD_DATA_VOLT] - intVal[AD_DATA_VOLT]) * 10)),
                    intVal[AD_DATA_AIRTEMP], abs((int) ((tmpVal[AD_DATA_AIRTEMP] - intVal[AD_DATA_AIRTEMP]) * 10)),
                    intVal[AD_DATA_CURR1], abs((int) ((tmpVal[AD_DATA_CURR1] - intVal[AD_DATA_CURR1]) * 100)),
                    intVal[AD_DATA_CURR2], abs((int) ((tmpVal[AD_DATA_CURR2] - intVal[AD_DATA_CURR2]) * 100)),
                    intVal[AD_DATA_CURR3], abs((int) ((tmpVal[AD_DATA_CURR3] - intVal[AD_DATA_CURR3]) * 100))
                    );

            ret = SD_Write(_str, size);
            if (ret == -1) {
                uart_debug_snd_buf_puts("---sd write error---\n");
                sd_logging_state = SD_STATE_ERR;
            } else {
                uart_debug_snd_buf_puts("---sd write success---\n");
            }
        	
        	write_count++;
        	if (10 < write_count)
        	{
        		SD_Save();
        	}
        }
    }
}

static void sd_control_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();

    for (;;) {
    	sdControl();

        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);
    }
}
/* --- グローバル関数群 ------------------------------------------------------*/
void sdCommInit(void) {
    // SD SPI初期化
    SPI_Init();

    // CD端子入力設定
    SD_CD_TRIS = 1;

    sd_logging_state = SD_STATE_DETECT;
	write_count = 0;
}

void create_sd_control_task(void) {
    BaseType_t ret;
	
//    ret = xTaskCreate(sd_control_task, "sd_control", TASK_STACK_SIZE, NULL,
//            TASK_PRIORITY, NULL);
	if (ret != pdPASS)
	{
		// タスク生成失敗
		uart_debug_snd_buf_puts("create_sd_control_task failed\r\n");
	}
}

unsigned char get_sd_logging_state(void)
{
	return sd_logging_state;
}

