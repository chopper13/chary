/*
 * File:   adc.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include "adc.h"
#include "FreeRTOS.h"
#include "task.h"
#include "timer.h"
#include "port.h"

#include "debug_port.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		1		// タスク優先度
#define TASK_STACK_SIZE		192		// スタックサイズ

enum {
    AD_CORRECT_GAIN = 0, //!< AD値のゲイン
    AD_CORRECT_OFFSET, //!< AD値のオフセット
    AD_CORRECT_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/
typedef struct tagAD_DATA_BUF {
    unsigned char wp;
    unsigned short dat[AD_DATA_MAX][AD_DATA_SIZE];
} AD_DATA_BUF;

/* --- 変数宣言 --------------------------------------------------------------*/
static AD_DATA_BUF adc_valList;
static unsigned int ad_ave[AD_DATA_MAX];

/* --- テーブル定義 ----------------------------------------------------------*/
const char *adc_name_list[AD_DATA_MAX] = {
    "curr1",
    "curr2",
    "oil  ",
    "temp ",
    "bat  ",
};

static const float ad_val_coefficient[AD_DATA_MAX][AD_CORRECT_MAX] = {

    { 0.0244141, -12.5}, // 電流1
    { 0.0244141, -12.5}, // 電流2
    { 1.0, 0.0}, // 油温
    { 0.322266, -60.0}, // 温度
    { 0.024707, 0.0}, // バッテリ
};

// バッテリ
// 27 / (180 + 27) = 0.13043478260869565217391304347826
// 3.3 / 1024 = 0.00322265625
// 0.00322265625 / 0.13043478260869565217391304347826 = 0.02470703125

// 電流 ±12.5[A] ACS711KLCTR-12AB-T
// 12.5 * 2 = 25.0
// 25.0 / 1024 = 0.0244140625

// 温度 LM61
// VO ＝ ( ＋ 10 mV/ ℃× T ℃ ) ＋ 600 mV
// 3.3 / 1024 = 0.00322265625
// 0.01 / 0.00322265625 = 3.1030303030303030303030303030303
// 1 / 3.1030303030303030303030303030303 = 0.322265625
// 0.6 / 0.01 = 60

/* --- 関数宣言 --------------------------------------------------------------*/
void __attribute__((interrupt(IPL0AUTO), vector(_ADC_VECTOR))) vAdcInterruptWrapper(void);

/* --- ローカル関数群 --------------------------------------------------------*/
void adc_ave_func(void) {
    int i, j;
    unsigned int sum;

    for (i = 0; i < AD_DATA_MAX; i++) {
        sum = 0;

        for (j = 0; j < AD_DATA_SIZE; j++) {
            sum += adc_valList.dat[i][j];
        }
        ad_ave[i] = sum / AD_DATA_SIZE;
    }
}

static void adc_ave_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();
    int val = 0;

    for (;;) {
        adc_ave_func();

        // 回転数算出はここで演算する
        input_capture_ave_func();

        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);

        if (val == 1) {
            val = 0;
            DEBUG_PORT1 = 0;
        } else {
            val = 1;
            DEBUG_PORT1 = 1;

        }
    }

}

/* --- グローバル関数群 ------------------------------------------------------*/
void adc_initialize(void) {
    // AD変換初期化
    AD1PCFG = 0xFF80;
    AD1CON1 = 0x0044; // SAMP bit = 0 ends sampling and starts converting
    AD1CHS = 0x00010000; // Connect RB2/AN2 as CH0 input;
    AD1CON2bits.CSCNA = 0x1;
    AD1CON2bits.SMPI = 0x4;
    AD1CSSL = 0x007C;


    AD1CON3 = 0x0100; // Manual Sample, TAD = internal 6 TPB
    AD1CON1SET = 0x8000; // Turn on the ADC

    // AD変換完了割り込み設定
    IPC6bits.AD1IP = 0x05;
    IPC6bits.AD1IS = 0x00;
    IFS1bits.AD1IF = 0;
    IEC1bits.AD1IE = 1;
}

float get_actual_val(unsigned char idx) {
    return ad_ave[idx] * ad_val_coefficient[idx][AD_CORRECT_GAIN]
            + ad_val_coefficient[idx][AD_CORRECT_OFFSET];
}

unsigned int get_ave_val(unsigned char idx) {
    return ad_ave[idx];
}

void create_adc_task(void) {
    BaseType_t ret;

    ret = xTaskCreate(adc_ave_task, "adc", TASK_STACK_SIZE, NULL,
            TASK_PRIORITY, NULL);
    if (ret != pdPASS) {
        // タスク生成失敗
        uart_debug_snd_buf_puts("create_adc_task failed\r\n");
    }
}

void vAdcInterruptHandler(void) {
    if (IFS1bits.AD1IF = 1) {
        IFS1bits.AD1IF = 0;
        if (AD1CON1bits.DONE) {
            adc_valList.wp++;
            if (adc_valList.wp >= AD_DATA_SIZE) {
                adc_valList.wp = 0;
            }

            adc_valList.dat[AD_DATA_CURR1][adc_valList.wp] = ADC1BUF2;
            adc_valList.dat[AD_DATA_CURR2][adc_valList.wp] = ADC1BUF3;
            adc_valList.dat[AD_DATA_OILTEMP][adc_valList.wp] = ADC1BUF4;
            adc_valList.dat[AD_DATA_AIRTEMP][adc_valList.wp] = ADC1BUF5;
            adc_valList.dat[AD_DATA_VOLT][adc_valList.wp] = ADC1BUF6;
        }
    }
}
