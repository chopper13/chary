/*
 * File:   adc.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __ADC_H_
#define __ADC_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define AD_DATA_SIZE	4	// 移動平均数

enum eAdDataIndex {
    AD_DATA_OILTEMP = 0,
    AD_DATA_VOLT,
    AD_DATA_AIRTEMP,
    AD_DATA_CURR1,
    AD_DATA_ACCURR,
    AD_DATA_CURR2,
    AD_DATA_CURR3,
    AD_DATA_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/
extern const char *adc_name_list[AD_DATA_MAX];

/* --- 関数宣言 --------------------------------------------------------------*/
extern void adc_initialize();
extern float get_actual_val(unsigned char idx);
extern unsigned int get_ave_val(unsigned char idx);
extern unsigned short get_comparator_pulse(void);
extern unsigned short get_rpm(void);
extern void create_adc_task(void);

#endif	// __ADC_H_

