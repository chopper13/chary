/*
 * File:   port.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __PORT_H_
#define __PORT_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/
enum eACCE {
    ACCE_X = 0,
    ACCE_Y,
    ACCE_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void port_initialize();
extern void input_capture_ave_func(void);
extern unsigned short get_rpm(void);
extern unsigned short get_acce(enum eACCE target);

#endif	// __PORT_H_

