/*
 * File:   adc.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include "Port.h"
#include "FreeRTOS.h"
#include "timer.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define RPM_PULSE_BUF_SIZE    8
#define ACCE_PULSE_BUF_SIZE    8

enum ePULSE {
    PULSE_BASE = 0,
    PULSE_UP,
    PULSE_MAX,
};


/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static unsigned short rpm_pulse_buf[RPM_PULSE_BUF_SIZE];
static unsigned short rpm_pulse_buf_old[RPM_PULSE_BUF_SIZE];
static unsigned char rpm_pulse_wp;
static unsigned short rpm_pulse_ave;
static unsigned short rpm_time_old;
static unsigned short acce_pulse_buf[ACCE_MAX][PULSE_MAX][ACCE_PULSE_BUF_SIZE];
static unsigned char acce_pulse_wp[ACCE_MAX];
static double acce_pulse_ave[ACCE_MAX];
static unsigned short acce_time_old[ACCE_MAX];


/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
void __attribute__((interrupt(IPL0AUTO), vector(_INPUT_CAPTURE_1_VECTOR))) vPortIC1InterruptWrapper(void);
void __attribute__((interrupt(IPL0AUTO), vector(_INPUT_CAPTURE_2_VECTOR))) vPortIC2InterruptWrapper(void);
void __attribute__((interrupt(IPL0AUTO), vector(_INPUT_CAPTURE_3_VECTOR))) vPortIC3InterruptWrapper(void);

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void port_initialize(void) {
    int i = 0;
    int j = 0;
    for (i = 0; i < RPM_PULSE_BUF_SIZE; i++) {
        rpm_pulse_buf[i] = 0;
        rpm_pulse_buf_old[i] = 0;
    }
    rpm_pulse_wp = 0;
    rpm_pulse_ave = 0;
    rpm_time_old = 0;

    for (i = 0; i < ACCE_MAX; i++) {
        for (j = 0; j < ACCE_PULSE_BUF_SIZE; j++) {
            acce_pulse_buf[i][PULSE_BASE][j] = 0;
            acce_pulse_buf[i][PULSE_UP][j] = 0;
        }
        acce_pulse_wp[i] = 0;
        acce_pulse_ave[i] = 0;
        acce_time_old[i] = 0;
    }

    // PORT変換初期化
    TRISDbits.TRISD8 = 1;
    TRISDbits.TRISD9 = 1;
    TRISDbits.TRISD10 = 1;

    // 加速度X
    IC1CON = 0;
    IC1CONbits.FEDGE = 1; // 立上がりエッジ指定
    IC1CONbits.C32 = 0;
    IC1CONbits.ICTMR = 1;
    IC1CONbits.ICI = 1; // 2回枚
    IC1CONbits.ICM = 6; // 指定エッジ後エッジ毎
    // 加速度Y
    IC2CON = 0;
    IC2CONbits.FEDGE = 1; // 立上がりエッジ指定
    IC2CONbits.C32 = 0;
    IC2CONbits.ICTMR = 1;
    IC2CONbits.ICI = 1; // 2回枚
    IC2CONbits.ICM = 6; // 指定エッジ後エッジ毎
    // RPM
    IC3CON = 0;
    IC3CONbits.C32 = 0;
    IC3CONbits.ICTMR = 1;
    IC3CONbits.ICI = 2; // 3回毎
    IC3CONbits.ICM = 2; // 立下りエッジ毎

    IC1CONbits.ON = 1;
    IC2CONbits.ON = 1;
    IC3CONbits.ON = 1;

    // IC割り込み設定
    IPC1bits.IC1IP = 0x06;
    IPC1bits.IC1IS = 0x00;
    IFS0bits.IC1IF = 0;
    IEC0bits.IC1IE = 1;

    IPC2bits.IC2IP = 0x06;
    IPC2bits.IC2IS = 0x00;
    IFS0bits.IC2IF = 0;
    IEC0bits.IC2IE = 1;

    IPC3bits.IC3IP = 0x06;
    IPC3bits.IC3IS = 0x00;
    IFS0bits.IC3IF = 0;
    IEC0bits.IC3IE = 1;
}

void input_capture_ave_func(void) {
    int i = 0;
    int j = 0;
    unsigned int sum = 0;
    unsigned int base_sum = 0;
    double ave = 0.0;
    double base_ave = 0.0;
    unsigned char old_value_comp = 1;

    // rpm
    for (i = 1; i < RPM_PULSE_BUF_SIZE; i++) {
        if (rpm_pulse_buf_old[i] != rpm_pulse_buf[i]) {
            old_value_comp = 0;
            sum += rpm_pulse_buf[i];
        }
        rpm_pulse_buf_old[i] == rpm_pulse_buf[i];
    }

    if (old_value_comp == 1) {
        rpm_pulse_ave = 0;
    } else {
        ave = sum / RPM_PULSE_BUF_SIZE;
        rpm_pulse_ave = (unsigned short) (9375000 / ave);
        //val = 60 / (comparator_pulse_ave * 0.0000064);
    }

    // 加速度
    sum = 0;
    for (i = 0; i < ACCE_MAX; i++) {
        for (j = 1; j < ACCE_PULSE_BUF_SIZE; j++) {
            base_sum += acce_pulse_buf[i][PULSE_BASE][j];
            sum += acce_pulse_buf[i][PULSE_UP][j];
        }
        base_ave = base_sum / ACCE_PULSE_BUF_SIZE;
        ave = sum / ACCE_PULSE_BUF_SIZE;
        acce_pulse_ave[i] = ((ave / base_ave) - 0.5) / 0.03;
    }
}

unsigned short get_rpm(void) {
    /*
    int val = 0;
    if (comparator_pulse_ave <= 0) {
        val = 0;
    } else {
        val = 9375000 / testr;
        //val = 9375000 / comparator_pulse_ave;
        //val = 60 / (comparator_pulse_ave * 0.0000064);
        if (0xFFFF < val) {
            val = 0xFFFF;
        }
    }
    return val;
     */
    return rpm_pulse_ave;
}

unsigned short get_acce(enum eACCE target) {
    return acce_pulse_ave[target];
}

void vPortIC1InterruptHandler(void) {
    // 周期時間が1パルス前の値を使用することになるが、
    // 周期はあまり変化しないはずなので問題なしとする
    unsigned short time = 0;

    if (IFS0bits.IC1IF = 1) {
        time = IC1BUF;
        acce_pulse_buf[ACCE_X][PULSE_BASE][acce_pulse_wp[ACCE_X]] = time - acce_time_old[ACCE_X];
        acce_pulse_buf[ACCE_X][PULSE_UP][acce_pulse_wp[ACCE_X]] = IC1BUF - time;
        acce_pulse_wp[ACCE_X]++;
        if (ACCE_PULSE_BUF_SIZE <= acce_pulse_wp[ACCE_X]) {
            acce_pulse_wp[ACCE_X] = 0;
        }
        acce_time_old[ACCE_X] = time;
    }

    acce_time_old[ACCE_X] = 0;
    IFS0bits.IC1IF = 0;
}

void vPortIC2InterruptHandler(void) {
    unsigned short time = 0;

    if (IFS0bits.IC2IF = 1) {
        time = IC2BUF;
        acce_pulse_buf[ACCE_Y][PULSE_BASE][acce_pulse_wp[ACCE_Y]] = time - acce_time_old[ACCE_Y];
        acce_pulse_buf[ACCE_Y][PULSE_UP][acce_pulse_wp[ACCE_Y]] = IC2BUF - time;
        acce_pulse_wp[ACCE_Y]++;
        if (ACCE_PULSE_BUF_SIZE <= acce_pulse_wp[ACCE_Y]) {
            acce_pulse_wp[ACCE_Y] = 0;
        }
        acce_time_old[ACCE_Y] = time;
    }

    acce_time_old[ACCE_Y] = 0;
    IFS0bits.IC2IF = 0;
}

void vPortIC3InterruptHandler(void) {
    unsigned short time = 0;
    if (IFS0bits.IC3IF = 1) {
        while (IC3CONbits.ICBNE == 1) {
            time = IC3BUF;
            rpm_pulse_buf[rpm_pulse_wp] = time - rpm_time_old;
            rpm_pulse_wp++;
            if (RPM_PULSE_BUF_SIZE <= rpm_pulse_wp) {
                rpm_pulse_wp = 0;
            }

            rpm_time_old = time;
        }
        IFS0bits.IC3IF = 0;
    }
}

