/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     displayControl.h
 *
 *     LCD描画 ヘッダ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef __DISPLAY_CONTROL_H_
#define __DISPLAY_CONTROL_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void create_lcd_write_task(void);
extern void lcd_write_clear(unsigned short Color);
extern void lcd_write_pixel(short Xpos, short Ypos, unsigned short Color);
extern void lcd_write_char(unsigned short xPoint, unsigned short yPoint, unsigned char letter, unsigned short Color1, unsigned short Color2);
extern void lcd_write_str(unsigned short xPoint, unsigned short yPoint, char *s, unsigned short Color1, unsigned short Color2);
extern void lcd_write_line(unsigned short xPoint1, unsigned short yPoint1, unsigned short xPoint2, unsigned short yPoint2, unsigned short Color);
extern void lcd_write_circle(unsigned short xPoint, unsigned short yPoint, unsigned short r, unsigned short color);
extern void lcd_write_seven_seg(unsigned char val, unsigned char panel);
// TODO JPEGのブロック出力関数も必要かも

#endif /* __DISPLAY_CONTROL_H_ */
