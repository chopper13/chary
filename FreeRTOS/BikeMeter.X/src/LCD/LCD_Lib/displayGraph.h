/*
 * File:   displayGraph.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __DISPLAY_GRAPH_H_
#define __DISPLAY_GRAPH_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "ADC/adc.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/
extern const unsigned short graph_line_color[AD_DATA_MAX];

/* --- 関数宣言 --------------------------------------------------------------*/
extern void Display_Graph_Reset();
extern void Display_Graph_Plot(float y[]);

#endif	// __DISPLAY_GRAPH_H_

