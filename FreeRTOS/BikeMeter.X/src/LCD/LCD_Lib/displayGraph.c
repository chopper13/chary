/*
 * File:   displayGraph.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include"displayGraph.h"
#include "colorlcd_libdsPICVH.h"


/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define GRAPH_BACK_COLOR	BLACK
#define GRAPH_GRID_COLOR	WHITE

#define GRAPH_GRID_SPRIT	5

#define GRAPH_AREA_START_X	0
#define GRAPH_AREA_START_Y	0
#define GRAPH_AREA_END_X	ENDCOL - 1
#define GRAPH_AREA_END_Y	207


/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static unsigned int graph_xline_cnt;
static unsigned int line_point_y_old[AD_DATA_MAX];

static unsigned char graph_show_flg[AD_DATA_MAX];

/* --- テーブル定義 ----------------------------------------------------------*/
const unsigned short graph_line_color[AD_DATA_MAX] = {
    0xFFF0,
    0xFF0F,
    0xF0FF,
    0x0FFF,
    0x041F,
    0xF800,
    0x07E0,
};


/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void Display_Graph_Reset() {
    int i, j;
    int drawAreaX = GRAPH_AREA_END_X - GRAPH_AREA_START_X;
    int drawAreaY = GRAPH_AREA_END_Y - GRAPH_AREA_START_Y;
    int gridLineIntarvalX = (drawAreaX / GRAPH_GRID_SPRIT) + 1;
    int gridLineIntarvalY = (drawAreaY / GRAPH_GRID_SPRIT) + 1;
    int gridDrawCntX;
    int gridDrawCntY;
    int gridDrawPointX = gridLineIntarvalY;
    int gridDrawPointY = gridLineIntarvalX;
    unsigned short gridColor;
    unsigned short gridColorX = GRAPH_GRID_COLOR;
    unsigned short gridColorY = GRAPH_GRID_COLOR;

    graph_xline_cnt = GRAPH_AREA_START_X;

    gridDrawCntY = 0;
    for (i = 0; i <= drawAreaY; i++) {
        Glcd_out(0x0020, GRAPH_AREA_START_Y + i);
        Glcd_out(0x0021, GRAPH_AREA_START_X);

        gridDrawPointY = gridLineIntarvalX;
        gridDrawCntX = 0;
        for (j = 0; j <= drawAreaX; j++) {
            if (gridDrawPointX != i) {
                gridColor = GRAPH_BACK_COLOR;
            } else {
                gridColor = gridColorX;
                gridDrawCntX++;
                if (gridDrawCntX == 5) {
                    if (gridColorX == GRAPH_BACK_COLOR) {
                        gridColorX = GRAPH_GRID_COLOR;
                    } else {
                        gridColorX = GRAPH_BACK_COLOR;
                    }

                    gridDrawCntX = 0;
                }
            }

            if (gridDrawPointY == j) {
                if (gridColor != GRAPH_GRID_COLOR) {
                    gridColor = gridColorY;
                }

                gridDrawPointY += gridLineIntarvalX;
            }

            Glcd_out(0x0022, gridColor); //
        }

        if (gridDrawPointX == i) {
            gridDrawPointX += gridLineIntarvalY;
        }

        gridDrawCntY++;
        if (gridDrawCntY == 5) {
            if (gridColorY == GRAPH_BACK_COLOR) {
                gridColorY = GRAPH_GRID_COLOR;
            } else {
                gridColorY = GRAPH_BACK_COLOR;
            }

            gridDrawCntY = 0;
        }

    }
}

void Display_Graph_Plot(float y[]) {
    int i;
    float point;
    unsigned int graph_y;
    unsigned int line_x;
    unsigned int line_y;
    unsigned int increment_x_count;
    unsigned int increment_y;

    if (GRAPH_AREA_END_X - GRAPH_AREA_START_X < graph_xline_cnt) {
        Display_Graph_Reset();
    }

    for (i = 0; i < AD_DATA_MAX; i++) {
        if (1.0 < y[i]) {
            point = 1.0;
        } else if (y[i] < 0.0) {
            point = 0.0;
        } else {
            point = y[i];
        }

        graph_y = GRAPH_AREA_END_Y - (unsigned int) ((GRAPH_AREA_END_Y - GRAPH_AREA_START_Y) * point);

        if (graph_xline_cnt != 0) {
            if (line_point_y_old[i] < graph_y) {
                increment_y = 1;
                increment_x_count = (unsigned int) ((graph_y - line_point_y_old[i]) / (float) 2.0);
            } else {
                increment_y = -1;
                increment_x_count = (unsigned int) ((line_point_y_old[i] - graph_y) / (float) 2.0);
            }

            line_x = graph_xline_cnt - 1;
            line_y = line_point_y_old[i];
            while (line_y != graph_y) {
                if (graph_show_flg[i] == 1) {
                    lcd_write_pixel(line_x, line_y, graph_line_color[i]);
                }
                line_y += increment_y;

                if (increment_x_count == line_y) {
                    line_x++;
                }
            }

        }
        if (graph_show_flg[i] == 1) {
            lcd_write_pixel(graph_xline_cnt, graph_y, graph_line_color[i]);
        }

        line_point_y_old[i] = graph_y;
    }

    graph_xline_cnt++;
}

void Display_Graph_SetShow(unsigned char flg[]) {
    int i;
    for (i = 0; i < AD_DATA_MAX; i++) {
        graph_show_flg[i] = flg[i];
    }

    Display_Graph_Reset();
}

