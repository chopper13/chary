/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     digitalMeter.c
 *
 *     digital meterコミュニケーション アプリ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "digitalMeter.h"
#include "colorlcd_libdsPICVH.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/
enum {
    SEG_LINE_A = 0,
    SEG_LINE_B,
    SEG_LINE_C,
    SEG_LINE_D,
    SEG_LINE_E,
    SEG_LINE_F,
    SEG_LINE_G,
    SEG_LINE_MAX,
};

typedef struct tagPoint {
    unsigned short x;
    unsigned short y;
} Point;

typedef struct tagSize {
    unsigned short x;
    unsigned short y;
} Size;

typedef struct tagsevenSegPanel {
    Point point;
    Size size;
    unsigned char thickness;
    unsigned short foreColor;
    unsigned short backColor;
} sevenSegPanel;

typedef struct tagsevenSegLine {
    Point startPoint;
    Point endPoint;
} sevenSegLine;


/* --- 変数宣言 --------------------------------------------------------------*/
static sevenSegLine segLine[SEG_MAX][SEG_LINE_MAX];
static char printValOld[SEG_MAX];


/* --- テーブル定義 ----------------------------------------------------------*/
static const sevenSegPanel segPanel[SEG_MAX] = {
    {
        {0, 20},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {80, 20},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {160, 20},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {0, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {80, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {170, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {0, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {80, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {160, 135},
        {70, 100}, 15, WHITE, BLACK
    },
    {
        {240, 140},
        {35, 60}, 10, WHITE, BLACK
    },
    {
        {280, 140},
        {35, 60}, 10, WHITE, BLACK
    },
};

/* --- 関数宣言 --------------------------------------------------------------*/
static void writeSegLine(sevenSegLine linePoint, unsigned char thickness, unsigned short color);

/* --- ローカル関数群 --------------------------------------------------------*/
static void writeSegLine(sevenSegLine linePoint, unsigned char thickness, unsigned short color) {
    unsigned short i = 0;
    short tmpThick = 0;
    Point startPoint = linePoint.startPoint;
    Point endPoint = linePoint.endPoint;
    Point tmpPoint;

    if (startPoint.x == endPoint.x) {
        // 縦表示

        if (endPoint.y < startPoint.y) {
            // 入替え
            tmpPoint = endPoint;
            endPoint = startPoint;
            startPoint = tmpPoint;
        }

        unsigned short j = 0;

        unsigned short y_diff = endPoint.y - startPoint.y;
        unsigned char tip_cnt = 0;
    	unsigned char start_tip_flg = 1;
        tmpThick = thickness % 2;

        for (i = 0; i < y_diff; i++) {
            Glcd_out(0x0020, startPoint.y + i); // 座標指定

        if (start_tip_flg == 1)
        {
        	if (tmpThick < thickness) {
        		// 太くする
            	tmpThick += 2;
            	if (thickness <= tmpThick) {
            	    tmpThick = thickness;
            		start_tip_flg = 0;
            	}
            	tip_cnt++;
        	}
        }
        	else {
           if (y_diff - i < tip_cnt) {
           	// 細くする
                tmpThick -= 2;
                if (tmpThick < 0) {
                    tmpThick = 0;
                }
            }
        	}

            Glcd_out(0x0021, startPoint.x - tmpThick / 2);
        

        for (j = 0; j < tmpThick; j++) {
            Glcd_out(0x0022, color);
        }

        }
    
} else if (startPoint.y == endPoint.y) {
    // 横表示

    if (endPoint.y < startPoint.y) {
        // 入替え
        tmpPoint = endPoint;
        endPoint = startPoint;
        startPoint = tmpPoint;
    }

    unsigned short j = 0;
    unsigned short tmpStartX = 0;

    Glcd_out(0x0020, startPoint.y); // 座標指定
    Glcd_out(0x0021, startPoint.x);
    tmpStartX = endPoint.x - startPoint.x;
    for (j = 0; j < tmpStartX; j++) {
        Glcd_out(0x0022, color);
    }

    tmpThick = thickness / 2;
    for (i = 0; i < tmpThick; i++) {
        Glcd_out(0x0020, startPoint.y + i); // 座標指定
        Glcd_out(0x0021, startPoint.x + i);
    	tmpStartX = endPoint.x - startPoint.x - (2 * i);
        for (j = 0; j < tmpStartX; j++) {
            Glcd_out(0x0022, color);
        }

        Glcd_out(0x0020, startPoint.y - i); // 座標指定
        Glcd_out(0x0021, startPoint.x + i);
        for (j = 0; j < tmpStartX; j++) {
            Glcd_out(0x0022, color);
        }
    }
} else {
    // 斜めは表示対象としない
}
}

/* --- グローバル関数群 ------------------------------------------------------*/
void sevenSegInit(void) {
    unsigned char i = 0;

    for (i = 0; i < SEG_MAX; i++) {
        unsigned char tmpThick = segPanel[i].thickness / 2;
        Point topLeft = segPanel[i].point;
        Point topRight = {segPanel[i].point.x + segPanel[i].size.x, segPanel[i].point.y};
        Point bottomLeft = {segPanel[i].point.x, segPanel[i].point.y + segPanel[i].size.y};
        Point bottomRight = {segPanel[i].point.x + segPanel[i].size.x, segPanel[i].point.y + segPanel[i].size.y};

        // 7セグメント表示場所計算
        segLine[i][SEG_LINE_A].startPoint.x = topLeft.x + tmpThick;
        segLine[i][SEG_LINE_A].startPoint.y = topLeft.y + tmpThick;
        segLine[i][SEG_LINE_A].endPoint.x = topRight.x - tmpThick;
        segLine[i][SEG_LINE_A].endPoint.y = topRight.y + tmpThick;

        segLine[i][SEG_LINE_B].startPoint = segLine[i][SEG_LINE_A].endPoint;
        segLine[i][SEG_LINE_B].endPoint.x = topRight.x - tmpThick;
        segLine[i][SEG_LINE_B].endPoint.y = topRight.y + (segPanel[i].size.y / 2);

        segLine[i][SEG_LINE_C].startPoint = segLine[i][SEG_LINE_B].endPoint;
        segLine[i][SEG_LINE_C].endPoint.x = bottomRight.x - tmpThick;
        segLine[i][SEG_LINE_C].endPoint.y = bottomRight.y - tmpThick;

        segLine[i][SEG_LINE_D].startPoint.x = bottomLeft.x + tmpThick;
        segLine[i][SEG_LINE_D].startPoint.y = bottomLeft.y - tmpThick;
        segLine[i][SEG_LINE_D].endPoint = segLine[i][SEG_LINE_C].endPoint;

        segLine[i][SEG_LINE_E].startPoint.x = bottomLeft.x + tmpThick;
        segLine[i][SEG_LINE_E].startPoint.y = topLeft.y + (segPanel[i].size.y / 2);
        segLine[i][SEG_LINE_E].endPoint = segLine[i][SEG_LINE_D].startPoint;

        segLine[i][SEG_LINE_F].startPoint = segLine[i][SEG_LINE_A].startPoint;
        segLine[i][SEG_LINE_F].endPoint = segLine[i][SEG_LINE_E].startPoint;

        segLine[i][SEG_LINE_G].startPoint = segLine[i][SEG_LINE_F].endPoint;
        segLine[i][SEG_LINE_G].endPoint = segLine[i][SEG_LINE_B].endPoint;

        // 前回表示時初期化
        printValOld[i] = -1;
    }
}

void setSevenSegVal(unsigned char val, unsigned char panel) {
    if (printValOld[panel] != val) {
        printValOld[panel] = val;

        switch (val) {
            case 0:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].backColor);
                break;

            case 1:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].backColor);
                break;

            case 2:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 3:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 4:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 5:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 6:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 7:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].backColor);
                break;

            case 8:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;

            case 9:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].foreColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].foreColor);
                break;
                
            case 10:
                writeSegLine(segLine[panel][SEG_LINE_A], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_B], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_C], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_D], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_E], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_F], segPanel[panel].thickness, segPanel[panel].backColor);
                writeSegLine(segLine[panel][SEG_LINE_G], segPanel[panel].thickness, segPanel[panel].backColor);
                break;

            default:
                break;
        }
    }
}

