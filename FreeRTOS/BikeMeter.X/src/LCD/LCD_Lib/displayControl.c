/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     displayControl.c
 *
 *     LCD描画
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayControl.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "digitalMeter.h"
#include "colorlcd_libdsPICVH.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
//#define TASK_MODE

#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		0		// タスク優先度
#define TASK_STACK_SIZE		192		// スタックサイズ

#define QUEUE_LENGTH				( 32 )		// 受信データキューサイズ

enum {
	LCD_WRITE_CLEAR,
    LCD_WRITE_PIXEL,
	LCD_WRITE_CHAR,
	LCD_WRITE_STR,
	LCD_WRITE_LINE,
	LCD_WRITE_CIRCLE,
	LCD_WRITE_SEVEN_SEG,
	LCD_WRITE_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/
typedef struct _LCD_CLEAR {
    unsigned short color;
} LCD_CLEAR;

typedef struct _LCD_PIXEL {
    short x;
	short y;
	unsigned short color;
} LCD_PIXEL;

typedef struct _LCD_CHAR {
    unsigned short x;
	unsigned short y;
	char letter;
	unsigned short front_color;
	unsigned short back_color;
} LCD_CHAR;

typedef struct _LCD_STR {
    unsigned short x;
	unsigned short y;
	char str[DISP_WRITE_LINE_CHAR_MAX + 1];
	unsigned short front_color;
	unsigned short back_color;
} LCD_STR;

typedef struct _LCD_LINE {
    unsigned short x1;
	unsigned short y1;
    unsigned short x2;
	unsigned short y2;
	unsigned short color;
} LCD_LINE;

typedef struct _LCD_CIRCLE {
    unsigned short x;
	unsigned short y;
    unsigned short r;
	unsigned short color;
} LCD_CIRCLE;

typedef struct _LCD_SEVEN_SEG {
    unsigned char val;
	unsigned char panel;
} LCD_SEVEN_SEG;

typedef union {
	LCD_CLEAR clear;
	LCD_PIXEL pixel;
	LCD_CHAR letter;
	LCD_STR str;
	LCD_LINE line;
	LCD_CIRCLE circle;
	LCD_SEVEN_SEG seven;
} LCD_WRITE_PARAM;


typedef struct _LCD_WRITE_CMD {
    unsigned char cmd;
    LCD_WRITE_PARAM param;
} LCD_WRITE_CMD;


/* --- 変数宣言 --------------------------------------------------------------*/
#ifdef TASK_MODE
static xTaskHandle xHandle;
static QueueHandle_t write_cmd_queue;
#endif

/* --- テーブル定義 ----------------------------------------------------------*/


/* --- 関数宣言 --------------------------------------------------------------*/


/* --- ローカル関数群 --------------------------------------------------------*/
#ifdef TASK_MODE
static void lcd_write(void)
{
	LCD_WRITE_CMD write_cmd;
	xQueueReceive(write_cmd_queue, &write_cmd, portMAX_DELAY);
	
	switch(write_cmd.cmd)
	{
	case LCD_WRITE_CLEAR:
		Glcd_Clear(write_cmd.param.clear.color);
		break;
		
	case LCD_WRITE_PIXEL:
		Glcd_Pixel(write_cmd.param.pixel.x, write_cmd.param.pixel.y, write_cmd.param.pixel.color);
		break;
		
	case LCD_WRITE_CHAR:
		Glcd_Char(write_cmd.param.letter.x, write_cmd.param.letter.y, write_cmd.param.letter.letter,
		write_cmd.param.letter.front_color, write_cmd.param.letter.back_color);
		break;
		
	case LCD_WRITE_STR:
		Glcd_Str(write_cmd.param.str.x, write_cmd.param.str.y, write_cmd.param.str.str,
		write_cmd.param.str.front_color, write_cmd.param.str.back_color);
		break;
		
	case LCD_WRITE_LINE:
		Glcd_Line(write_cmd.param.line.x1, write_cmd.param.line.y1, write_cmd.param.line.x2,
		write_cmd.param.line.y2, write_cmd.param.line.color);
		break;
		
	case LCD_WRITE_CIRCLE:
		Glcd_Circle(write_cmd.param.circle.x, write_cmd.param.circle.y, 
			write_cmd.param.circle.r, write_cmd.param.circle.color);
		break;
		
	case LCD_WRITE_SEVEN_SEG:
		setSevenSegVal(write_cmd.param.seven.val, write_cmd.param.seven.panel);
		break;
		
	default:
		break;
	}
}

static void lcd_write_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();

    for (;;) {
    	lcd_write();

//        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);
    }
}
#endif

/* --- グローバル関数群 ------------------------------------------------------*/
void create_lcd_write_task(void)
{
#ifdef TASK_MODE
	write_cmd_queue = xQueueCreate( QUEUE_LENGTH, sizeof(LCD_WRITE_CMD));
	if (write_cmd_queue == 0)
	{
		// キュー生成失敗
		return;
	}
	
	BaseType_t ret;
    ret = xTaskCreate(lcd_write_task, "lcd_write", TASK_STACK_SIZE, NULL,
            TASK_PRIORITY, xHandle);
		if (ret != pdPASS)
	{
		// タスク生成失敗
		uart_debug_snd_buf_puts("create_lcd_write_task failed\r\n");
	}
#endif
}

void lcd_write_clear(unsigned short Color)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_CLEAR;
	write_cmd.param.clear.color = Color;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Clear(Color);
#endif
}

void lcd_write_pixel(short Xpos, short Ypos, unsigned short Color)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_PIXEL;
	write_cmd.param.pixel.x = Xpos;
	write_cmd.param.pixel.y = Ypos;
	write_cmd.param.pixel.color = Color;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Pixel(Xpos, Ypos, Color);
#endif
}

void lcd_write_char(unsigned short xPoint, unsigned short yPoint, unsigned char letter, unsigned short Color1, unsigned short Color2)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_CHAR;
	write_cmd.param.letter.x = xPoint;
	write_cmd.param.letter.y = yPoint;
	write_cmd.param.letter.letter = letter;
	write_cmd.param.letter.front_color = Color1;
	write_cmd.param.letter.back_color = Color2;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Char(xPoint, yPoint, letter, Color1, Color2);
#endif
}

void lcd_write_str(unsigned short xPoint, unsigned short yPoint, char *s, unsigned short Color1, unsigned short Color2)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_STR;
	write_cmd.param.str.x = xPoint;
	write_cmd.param.str.y = yPoint;
    memset(&write_cmd.param.str.str, *s, DISP_WRITE_LINE_CHAR_MAX + 1);
	write_cmd.param.str.front_color = Color1;
	write_cmd.param.str.back_color = Color2;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Str(xPoint, yPoint, s, Color1, Color2);
#endif
}

void lcd_write_line(unsigned short xPoint1, unsigned short yPoint1, unsigned short xPoint2, unsigned short yPoint2, unsigned short Color)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_LINE;
	write_cmd.param.line.x1 = xPoint1;
	write_cmd.param.line.y1 = yPoint1;
	write_cmd.param.line.x2 = xPoint2;
	write_cmd.param.line.y2 = yPoint2;
	write_cmd.param.line.color = Color;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Line(xPoint1, yPoint1, xPoint2, yPoint2, Color);
#endif
}

void lcd_write_circle(unsigned short xPoint, unsigned short yPoint, unsigned short r, unsigned short color)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_CIRCLE;
	write_cmd.param.circle.x = xPoint;
	write_cmd.param.circle.y = yPoint;
	write_cmd.param.circle.r = r;
	write_cmd.param.circle.color = color;
	
	xQueueSend(write_cmd_queue, &write_cmd,portMAX_DELAY);
#else
	Glcd_Circle(xPoint, yPoint, r, color);
#endif
}

void lcd_write_seven_seg(unsigned char val, unsigned char panel)
{
#ifdef TASK_MODE
	LCD_WRITE_CMD write_cmd;
	
	write_cmd.cmd = LCD_WRITE_SEVEN_SEG;
	write_cmd.param.seven.val = val;
	write_cmd.param.seven.panel = panel;
	
	xQueueSend(write_cmd_queue, &write_cmd, portMAX_DELAY);
#else
	setSevenSegVal(val, panel);
#endif
}

