/* 
 * File:   colorlcd_libdsPICVH.h
 * Author: ei
 *
 * Created on 2014/01/16, 20:40
 */

#ifndef __COLORLCD_LIBDSPICVH_H_
#define	__COLORLCD_LIBDSPICVH_H_

//colorlcd_libdsPICVH.h
//このグラフィックライブラリは　後閑哲也さんが設計されたdsPIC用グラフィックライブラリを
//PIC32MX 、　データバス：８ビットパラレル、　パラレルマスターポート（PMP)モジュール 用に
//変更したものです。
/*************************************************
 *  グラフィック液晶表示器用ライブラリ　ヘッダ
 *               ポートの定義
 *               関数プロトタイプ
 **************************************************/

#include <xc.h>

/* LCD Ports define */


//#define LCD_CS          LATBbits.LATB3
//#define LCD_RS          LATBbits.LATB4
//#define LCD_WR          LATBbits.LATB14
#define LCD_RESET       LATBbits.LATB12

#define ENDCOL     320  // X
#define ENDROW     240  // Y
#define XChar             (short)((ENDCOL) / 16)
#define YLine             (short)((ENDROW) / 16)

#define DISP_WRITE_LINE_CHAR_MAX	20	// 1行に表示可能な最大文字数

/*******************************************
 *  16bit Color Difinition
 *******************************************/
#define WHITE           0xFFFF
#define BLACK           0x0000
#define RED             0xF800
#define GREEN           0x07E0
#define BLUE            0x001F
#define CYAN            0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define BROWN           0x8000
#define ORANGE  0xFC00
#define PERPLE  0x8010
#define COBALT  0x041F
#define GLAY  0xAD55

/***  関数プロトタイプ ****/
void Glcd_out(unsigned short index, unsigned short data);
void Glcd_Init(void);
void Glcd_Clear(unsigned short Color);
void Glcd_Pixel(short Xpos, short Ypos, unsigned short Color);
void Glcd_Char(unsigned short xPoint, unsigned short yPoint, unsigned char letter, unsigned short Color1, unsigned short Color2);
void Glcd_Str(unsigned short xPoint, unsigned short yPoint, char *s, unsigned short Color1, unsigned short Color2);
void Glcd_Line(unsigned short xPoint1, unsigned short yPoint1, unsigned short xPoint2, unsigned short yPoint2, unsigned short Color);
void Glcd_Circle(unsigned short xPoint, unsigned short yPoint, unsigned short r, unsigned short color);





#endif	/* __COLORLCD_LIBDSPICVH_H_ */

