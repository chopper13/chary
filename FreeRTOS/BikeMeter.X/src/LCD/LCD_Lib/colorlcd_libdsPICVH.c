
//colorlcd_libdsPICVH.c
//このグラフィックライブラリは　後閑哲也さんが設計されたdsPIC用グラフィックライブラリを
//PIC32MX 、　データバス：８ビットパラレル、　パラレルマスターポート（PMP)モジュール 用に
//変更したものです。
/********************************************************
 *  カラーグラフィックLCD(240ｘ320ドット）用ライブラリ
 *   横表示で設定
 *       Glcd_Init();            // 初期化
 *       Glcd_Clear();           // 全画面消去
 *       Glcd_Pixel();           // 1ドット表示
 *       Glcd_Char();            // ANK文字表示
 *       Glcd_Str();             // ANK文字列表示
 *       Glcd_Line();            // 直線描画
 *       Glcd_Circle();          // 円描画
 *********************************************************/

#include "colorlcd_libdsPICVH.h"
#include "tri_S_Font16x16ASCII.h"         //ASCII 16x16dot

/************************************
 * レジスタ設定とデータ出力関数
 *************************************/
void Glcd_out(unsigned short index, unsigned short data) {
    // Index上位バイト出力
    PMADDR = 0;
    while(PMMODEbits.BUSY);
    PMDIN = ((index >> 8) & 0x00FF);

    // Index下位バイト出力
    while(PMMODEbits.BUSY);
    PMDIN = (index & 0x00FF);
    
    // Data上位バイト出力
    PMADDR = 1;
    while(PMMODEbits.BUSY);
    PMDIN = (data >> 8) & 0x00FF;
    
    // Index下位バイト出力
    while(PMMODEbits.BUSY);
    PMDIN = (data & 0x00FF);
}

/****************************
 * LCD初期化関数
 *****************************/
void Glcd_Init(void) {
    // RB12 初期化
    TRISBbits.TRISB12 = 0;

      PMCON = 0; //PMCONレジスタ　リセット
    PMMODE = 0; //PMMODEレジスタ　リセット
    PMAEN = 0; //PMAENレジスタ　リセット

    PMMODEbits.MODE = 2;

    PMMODEbits.WAITB = 0;
	PMMODEbits.WAITM = 0;
    PMMODEbits.WAITE = 0;

    PMCONbits.PTRDEN = 1;
    PMCONbits.PTWREN = 1;

    // とりあえず、CSはポート制御でLo固定にしておく
    // できればPMPのCS機能を使いたい
    PMAENbits.PTEN0 = 1;
    PMAENbits.PTEN14 = 0;
    PMCONbits.CSF = 0;

    PMCONbits.CS1P = 0;
    PMADDRbits.CS1 = 0;

    PMCONbits.ON = 1; //パラレルマスタモードON // enable PMP

        TRISDbits.TRISD11 = 0;
     LATDbits.LATD11 = 0;
     
    delay_ms(20);

    LCD_RESET = 1; // Reset LCD
    delay_ms(1);
    LCD_RESET = 0; // Reset LCD
    delay_ms(10); // 1msec wait
    LCD_RESET = 1; // Clear RESET
    //        delay_ms(25);                           // 25msec wait
    delay_ms(50); // 50msec wait

    Glcd_out(0x00E3, 0x3008); // Set internal timing
    Glcd_out(0x00E7, 0x0012); // Set internal timing
    Glcd_out(0x00EF, 0x1231); // Set internal timing
    Glcd_out(0x0001, 0x0000); // set SS and SM bit
    Glcd_out(0x0002, 0x0700); // set line inversion
    //Glcd_out(0x0003, 0x1018); // set GRAM write direction and BGR=1, 16bit color
    //Glcd_out(0x0003, 0x1030);       // set GRAM write direction and BGR=1, 16bit color
    Glcd_out(0x0003, 0x1038); // set GRAM write direction and BGR=1, 16bit color
    Glcd_out(0x0004, 0x0000); // Resize register
    Glcd_out(0x0008, 0x0207); // set the back porch and front porch
    Glcd_out(0x0009, 0x0000); // set non-display area refresh cycle ISC[3:0]
    Glcd_out(0x000A, 0x0000); // FMARK function
    Glcd_out(0x000C, 0x0000); // RGB interface setting
    Glcd_out(0x000D, 0x0000); // Frame marker Position
    Glcd_out(0x000F, 0x0000); // RGB interface polarity
    /********* 電源オンシーケンス  **************/
    Glcd_out(0x0010, 0x0000); // SAP, BT[3:0], AP[2:0], DSTB, SLP, STB
    Glcd_out(0x0011, 0x0007); // DC1[2:0], DC0[2:0], VC[2:0]
    Glcd_out(0x0012, 0x0000); // VREG1OUT voltage
    Glcd_out(0x0013, 0x0000); // VDV[4:0] for VCOM amplitude
    delay_ms(200); // コンデンサ放電待ち
    Glcd_out(0x0010, 0x1490); // SAP, BT[3:0], AP[2:0], DSTB, SLP, STB
    Glcd_out(0x0011, 0x0227); // R11h=0x0221 at VCI=3.3V ,DC1[2:0], DC0[2:0], VC[2:0]
    delay_ms(50); // 遅延 50ms
    Glcd_out(0x0012, 0x001c); // External reference voltage= Vci;
    delay_ms(50); // 遅延 50ms
    Glcd_out(0x0013, 0x0A00); // R13=0F00 when R12=009E;VDV[4:0] for VCOM amplitude
    Glcd_out(0x0029, 0x000F); // R29=0019 when R12=009E;VCM[5:0] for VCOMH//0012//
    Glcd_out(0x002B, 0x000D); // Frame Rate = 91Hz
    delay_ms(50); // 遅延 50ms
    Glcd_out(0x0020, 0x0000); // GRAM horizontal Address
    Glcd_out(0x0021, 0x0000); // GRAM Vertical Address
    /**********  ガンマ補正  **********/
    Glcd_out(0x0030, 0x0000);
    Glcd_out(0x0031, 0x0203);
    Glcd_out(0x0032, 0x0001);
    Glcd_out(0x0035, 0x0205);
    Glcd_out(0x0036, 0x030C);
    Glcd_out(0x0037, 0x0607);
    Glcd_out(0x0038, 0x0405);
    Glcd_out(0x0039, 0x0707);
    Glcd_out(0x003C, 0x0502);
    Glcd_out(0x003D, 0x1008);
    /*********** GRAM領域設定 *********/
    Glcd_out(0x0050, 0x0000); // Horizontal GRAM Start Address
    Glcd_out(0x0051, 0x00EF); // Horizontal GRAM End Address
    Glcd_out(0x0052, 0x0000); // Vertical GRAM Start Address
    Glcd_out(0x0053, 0x013F); // Vertical GRAM Start Address
    Glcd_out(0x0060, 0xA700); // Gate Scan Line 横表示用
    Glcd_out(0x0061, 0x0001); // NDL,VLE, REV
    Glcd_out(0x006A, 0x0000); // set scrolling line
    /************* 部分表示制御 ************/
    Glcd_out(0x0080, 0x0000); // 部分表示1位置
    Glcd_out(0x0081, 0x0000); // 部分表示1RAM開始アドレス
    Glcd_out(0x0082, 0x0000); // 部分表示1RAM終了アドレス
    Glcd_out(0x0083, 0x0000); // 部分表示2位置
    Glcd_out(0x0084, 0x0000); // 部分表示2RAN開始アドレス
    Glcd_out(0x0085, 0x0000); // 部分表示2RAM終了アドレス
    /************** パネル制御 ************/
    Glcd_out(0x0090, 0x0010); // 1ラインクロック数
    Glcd_out(0x0092, 0x0600); // ゲートオーバーラップクロック数
    Glcd_out(0x0093, 0x0003); // 出力タイミング
    Glcd_out(0x0095, 0x0110); // RGBの1ラインクロック数
    Glcd_out(0x0097, 0x0000); // 出力タイミング
    Glcd_out(0x0098, 0x0000); // 出力タイミング
    /***** 表示制御 *****/
    Glcd_out(0x0007, 0x0133); // 262K color and display ON
}

/***************************
 * 画面消去関数
 * 消去用色指定あり
 ****************************/
void Glcd_Clear(unsigned short Color) {
    int i, j;

    Glcd_out(0x0020, 0); // 原点にセット
    Glcd_out(0x0021, 0);
    for (j = 0; j < ENDROW; j++) { // Y軸全部
        for (i = 0; i < ENDCOL; i++) { // X軸全部
            Glcd_out(0x0022, Color); // 同色で埋める
        }
    }
}

/***********************************
 *  １ピクセル表示関数
 *  座標は(0,0)-(319,239)
 ***********************************/
void Glcd_Pixel(short Xpos, short Ypos, unsigned short Color) {
    if ((Xpos < ENDCOL) && (Ypos < ENDROW)) {
        Glcd_out(0x0020, Ypos); // 座標指定
        Glcd_out(0x0021, Xpos);
        Glcd_out(0x0022, Color); // ドット表示
    }
}
/***************************
 *  直線描画関数
 ***************************/
#define abs(a)  (((a)>0) ? (a) : -(a))

void Glcd_Line(unsigned short xPoint1, unsigned short yPoint1, unsigned short xPoint2, unsigned short yPoint2, unsigned short Color) {
    short steep, t;
    short deltax, deltay, error;
    short x, y;
    short ystep;

    //	yPoint1 = ENDROW - yPoint1 - 1; // Y座標反転
    //	yPoint2 = ENDROW - yPoint2 - 1;
    yPoint1 = ENDROW - yPoint1; // Y座標反転
    yPoint2 = ENDROW - yPoint2;
    /// 差分の大きいほうを求める
    steep = (abs(yPoint2 - yPoint1) > abs(xPoint2 - xPoint1));
    /// ｘ、ｙの入れ替え
    if (steep) {
        t = xPoint1;
        xPoint1 = yPoint1;
        yPoint1 = t;
        t = xPoint2;
        xPoint2 = yPoint2;
        yPoint2 = t;
    }
    if (xPoint1 > xPoint2) {
        t = xPoint1;
        xPoint1 = xPoint2;
        xPoint2 = t;
        t = yPoint1;
        yPoint1 = yPoint2;
        yPoint2 = t;
    }
    deltax = xPoint2 - xPoint1; // 傾き計算
    deltay = abs(yPoint2 - yPoint1);
    error = 0;
    y = yPoint1;
    /// 傾きでステップの正負を切り替え
    if (yPoint1 < yPoint2) ystep = 1;
    else ystep = -1;
    /// 直線を点で描画
    for (x = xPoint1; x <= xPoint2; x++) {
        if (steep) Glcd_Pixel(y, x, Color);
        else Glcd_Pixel(x, y, Color);
        error += deltay;
        if ((error << 1) >= deltax) {
            y += ystep;
            error -= deltax;
        }
    }
}

/*************************************
 *  円を描く関数
 *  中心点と半径を指定
 *  (Fussyさんのアルゴリズムを使用)
 **************************************/
void Glcd_Circle(unsigned short xPoint, unsigned short yPoint, unsigned short r, unsigned short color) {
    int x = r;
    int y = 0;
    int F = -2 * r + 3;

    while (x >= y) {
        Glcd_Pixel(xPoint + x, yPoint + y, color);
        Glcd_Pixel(xPoint - x, yPoint + y, color);
        Glcd_Pixel(xPoint + x, yPoint - y, color);
        Glcd_Pixel(xPoint - x, yPoint - y, color);
        Glcd_Pixel(xPoint + y, yPoint + x, color);
        Glcd_Pixel(xPoint - y, yPoint + x, color);
        Glcd_Pixel(xPoint + y, yPoint - x, color);
        Glcd_Pixel(xPoint - y, yPoint - x, color);
        if (F >= 0) {
            x--;
            F -= 4 * x;
        }
        y++;
        F += 4 * y + 2;
    }
}

/*****************************************
 *  ANK文字表示関数 16x16ドット
 ******************************************/
void Glcd_Char(unsigned short xPoint, unsigned short yPoint, unsigned char letter, unsigned short Color1, unsigned short Color2) {
    unsigned char j, i, Mask;

    if ((xPoint < ENDCOL) && (yPoint < ENDROW)) { // 範囲チェック
    	for (j = 0; j < 16; j++) {
    		Glcd_out(0x0020, yPoint + j); // 座標指定
        	Glcd_out(0x0021, xPoint);
    		
    		Mask = 0x80;
    		for (i = 0; i < 8; i++) {
                if ((tri_S_uchrFont16x16ASCII[(letter * 32) + (j * 2)] & Mask) != 0)
    				Glcd_out(0x0022, Color1);
                else
    				Glcd_out(0x0022, Color2); //背景色
                Mask = Mask >> 1;
            }
    		
    		Mask = 0x80;
    		for (i = 0; i < 8; i++) {
                if ((tri_S_uchrFont16x16ASCII[(letter * 32) + (j * 2) + 1] & Mask) != 0)
    				Glcd_out(0x0022, Color1);
                else
    				Glcd_out(0x0022, Color2); //背景色
                Mask = Mask >> 1;
            }

    	}
    }
}

/******************************
 *   文字列描画関数
 ******************************/
void Glcd_Str(unsigned short xPoint, unsigned short YPoint, char *s, unsigned short Color1, unsigned short Color2) {
    
	// TODO DISP_WRITE_LINE_CHAR_MAXの制限がほしいかな
	while (*s) {
        Glcd_Char((xPoint++) * 16, YPoint * 16, *s++, Color1, Color2);
    }
}

