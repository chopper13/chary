/*
 * File:   lcd_touch.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include <string.h>
#include "spi.h"
#include "lcd_touch.h"
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "colorlcd_libdsPICVH.h"

#include "tiny_sprintf.h"
#include "serial.h"
#include "delay.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define TOUCH_DEBUG
#define TOUCH_DEBUG_ORISINAL_VAL

#define PENIRQ_PORT_TRIS	TRISCbits.TRISC13
#define PENIRQ_PORT			PORTCbits.RC13
#define BUSY_PORT_TRIS	TRISCbits.TRISC14
#define BUSY_PORT			PORTCbits.RC14

#define TOUCH_DIFF_TOLERANCE		20
#define TOUCH_RCV_BUF_SIZE			(0x20)
#define TOUCH_MOVE					150
#define TOUCH_MARGIN				100

#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		3		// タスク優先度
#define TASK_STACK_SIZE		192		// スタックサイズ

#define TOUCH_AVE_CNT		2		// タッチセンサ取得回数

#define QUEUE_LENGTH				( 32 )		// 受信データキューサイズ

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/*
 *  バッファ
 */
typedef struct _TOUCH_RCV_BUF {
    unsigned short wp;
    unsigned char overFlow;
    TOUCH_POINT dat[TOUCH_RCV_BUF_SIZE];
} TOUCH_RCV_BUF;

/* --- 変数宣言 --------------------------------------------------------------*/
static const float x_gain = -1.73913;
static const float x_offset = 1259.13;
static const float y_gain = 2.608696;
static const float y_offset = -2566.96;
//static const float x_gain = -0.19116f;
//static const float x_offset = 353.644f;
//static const float y_gain = -0.16021f;
//static const float y_offset = 281.976f;

static TOUCH_RCV_BUF touch_rcv_buf;
static volatile TOUCH_COMMAND now_touch_command;

static QueueHandle_t touch_queue;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static TOUCH_POINT get_ads7843_coordinate(void) {
    // CS on
	touch_cs_select(0);

    unsigned char buf[2];
    unsigned short tmpPoint = 0;
    unsigned short oldPoint = 0;
    unsigned char nearFlg = 0;
    TOUCH_POINT point;

    //portDISABLE_INTERRUPTS();
    // X座標
    SPI3BUF = 0x92; // 1 001 0 0 10 12bit 差動
    while (!SPI3STATbits.SPIRBF);
    buf[0] = SPI3BUF; // ダミー読み出し
    //   while (BUSY_PORT == 1);
    //  delay_us(1000);
    // TODO Busyポートで無限待ちしているみたい
    SPI3BUF = 0x00;
    while (!SPI3STATbits.SPIRBF);
    buf[0] = SPI3BUF;

    while (nearFlg == 0) {
        SPI3BUF = 0x92; // 1 001 0 0 10 12bit 差動
        while (!SPI3STATbits.SPIRBF);
        buf[1] = SPI3BUF;

        tmpPoint = ((unsigned short) buf[0] << 4) + ((buf[1] >> 4) & 0x0F);
        if ((tmpPoint - oldPoint) < TOUCH_DIFF_TOLERANCE
        || (oldPoint - tmpPoint) < TOUCH_DIFF_TOLERANCE) {
            point.x = tmpPoint;
            nearFlg = 1;
            break;
        }
        oldPoint = tmpPoint;
        //    delay_us(1000);
        //     while (BUSY_PORT == 1);

        SPI3BUF = 0x00;
        while (!SPI3STATbits.SPIRBF);
        buf[0] = SPI3BUF;
    }
    // Y座標
    nearFlg = 0;
    SPI3BUF = 0xD2; // 1 101 0 0 10 12bit 差動
    while (!SPI3STATbits.SPIRBF);
    buf[0] = SPI3BUF; // ダミー読み出し
    //   delay_us(1000);
    //  while (BUSY_PORT == 1);

    SPI3BUF = 0x00;
    while (!SPI3STATbits.SPIRBF);
    buf[0] = SPI3BUF;

    while (nearFlg == 0) {
        SPI3BUF = 0xD2; // 1 101 0 0 10 12bit 差動
        while (!SPI3STATbits.SPIRBF);
        buf[1] = SPI3BUF;

        tmpPoint = ((unsigned short) buf[0] << 4) + ((buf[1] >> 4) & 0x0F);
           if ((tmpPoint - oldPoint) < TOUCH_DIFF_TOLERANCE
        || (oldPoint - tmpPoint) < TOUCH_DIFF_TOLERANCE) {
            point.y = tmpPoint;
            nearFlg = 1;
            break;
        }
        oldPoint = tmpPoint;
        //    delay_us(1000);
        //    while (BUSY_PORT == 1);

        SPI3BUF = 0x00;
        while (!SPI3STATbits.SPIRBF);
        buf[0] = SPI3BUF;
    }
    // portENABLE_INTERRUPTS();

    // CS off
	touch_cs_select(1);

#ifdef TOUCH_DEBUG_ORISINAL_VAL
    char test[30];
    sprintf(test, "ori x:%d y;%d\r\n", point.x, point.y);
    uart_debug_snd_buf_puts(test);
#endif

    return point;
}

static TOUCH_POINT convert_lcd_coordinate(TOUCH_POINT oriVal) {
    TOUCH_POINT retVal;

    // X LCD座標に変換
    short tmpVal = (short) (oriVal.x * x_gain + x_offset);
    if (tmpVal < 0) {
        retVal.x = 0;
#ifdef TOUCH_DEBUG
        uart_debug_snd_buf_puts("x = 0\r\n");
#endif
    } else if (ENDCOL < tmpVal) {
        retVal.x = ENDCOL;
#ifdef TOUCH_DEBUG
        uart_debug_snd_buf_puts("x = ENDCOL\r\n");
#endif
    } else {
        retVal.x = (unsigned short) tmpVal;
#ifdef TOUCH_DEBUG
        char test[30];
        sprintf(test, "x = %d\r\n", retVal.x);
        uart_debug_snd_buf_puts(test);
#endif
    }

    // Y LCD座標に変換
    tmpVal = (short) (oriVal.y * y_gain + y_offset);
    if (tmpVal < 0) {
        retVal.y = 0;
#ifdef TOUCH_DEBUG
        uart_debug_snd_buf_puts("y = 0\r\n");
#endif
    } else if (ENDROW < tmpVal) {
        retVal.y = ENDROW;
#ifdef TOUCH_DEBUG
        uart_debug_snd_buf_puts("y = ENDROW\r\n");
#endif
    } else {
        retVal.y = (unsigned short) tmpVal;
#ifdef TOUCH_DEBUG
        char test[30];
        sprintf(test, "y = %d\r\n", retVal.y);
        uart_debug_snd_buf_puts(test);
#endif
    }

    return retVal;
}

static TOUCH_COMMAND touch_gesture(void) {
    static unsigned char penReqOld = 0;
    TOUCH_COMMAND retVal;
    TOUCH_COMMAND oriVal;

    if (PENIRQ_PORT == 0) {
        // PENIRQ端子がLowの場合はタッチ検出として値を取得しにいく

        retVal.command = TOUCH_NOW;

        if (penReqOld == 0) {
#ifdef TOUCH_DEBUG
            uart_debug_snd_buf_puts("touch start\r\n");
#endif
            // タッチ開始
            penReqOld = 1;
            touch_rcv_buf.wp = 0;
            touch_rcv_buf.overFlow = 0;
        }

        // ADS7843から座標取得
        oriVal.p = get_ads7843_coordinate();

        // LCD内の座標に変換
        retVal.p = convert_lcd_coordinate(oriVal.p);
    } else {
        // タッチセンサ未取得時は座標を0とする
        retVal.p.x = 0;
        retVal.p.y = 0;
    }

    if (retVal.p.x != 0 && retVal.p.y != 0) {
        // タッチセンサ取得値をバッファに格納
        touch_rcv_buf.dat[touch_rcv_buf.wp].x = oriVal.p.x;
        touch_rcv_buf.dat[touch_rcv_buf.wp].y = oriVal.p.y;
        touch_rcv_buf.wp++;
        if (TOUCH_RCV_BUF_SIZE <= touch_rcv_buf.wp) {
            touch_rcv_buf.wp = 0;
            touch_rcv_buf.overFlow = 1;
        }
    } else {
        if (penReqOld == 1) {
#ifdef TOUCH_DEBUG
            uart_debug_snd_buf_puts("touch end\r\n");
#endif
            // タッチ終了
            penReqOld = 0;

            // タッチバッファの確認
            // バッファ読み込み開始位置決定
            unsigned short rp;
            if (touch_rcv_buf.overFlow == 0) {
                rp = 0;
            } else {
                if (TOUCH_RCV_BUF_SIZE <= touch_rcv_buf.wp + 1) {
                    rp = 0;
                } else {
                    rp = touch_rcv_buf.wp + 1;
                }
            }

            unsigned short xMax = touch_rcv_buf.dat[rp].x;
            unsigned short xMin = xMax;
            unsigned short xOld = xMax;
            unsigned short yMax = touch_rcv_buf.dat[rp].y;
            unsigned short yMin = yMax;
            unsigned short yOld = yMax;

            // バッファを見てタッチの軌跡を判定
            unsigned short tmpRp = rp;
            while (tmpRp != touch_rcv_buf.wp) {
                // 四方向確認
                // 最大移動場所を確認
                if (xMax < touch_rcv_buf.dat[tmpRp].x) {
                    xMax = touch_rcv_buf.dat[tmpRp].x;
                }
                if (touch_rcv_buf.dat[tmpRp].x < xMin) {
                    xMin = touch_rcv_buf.dat[tmpRp].x;
                }
                if (yMax < touch_rcv_buf.dat[tmpRp].y) {
                    yMax = touch_rcv_buf.dat[tmpRp].y;
                }
                if (touch_rcv_buf.dat[tmpRp].y < xMin) {
                    yMin = touch_rcv_buf.dat[tmpRp].y;
                }

                // リードポイントインクリメント
                tmpRp++;
                if (TOUCH_RCV_BUF_SIZE <= tmpRp) {
                    tmpRp = 0;
                }
            }

            // 移動量が多い方向を検出
            if ((TOUCH_MOVE < (yMax - yOld))
                    && (TOUCH_MARGIN > (yOld - yMin))
                    && (TOUCH_MARGIN > (xMax - xOld))
                    && (TOUCH_MARGIN > (xOld - xMin))) {
                // 下
                retVal.command = TOUCH_COMMAND_DOWN;
#ifdef TOUCH_DEBUG
                uart_debug_snd_buf_puts("TOUCH_COMMAND_DOWN\r\n");
#endif
            } else if ((TOUCH_MARGIN > (yMax - yOld))
                    && (TOUCH_MOVE < (yOld - yMin))
                    && (TOUCH_MARGIN > (xMax - xOld))
                    && (TOUCH_MARGIN > (xOld - xMin))) {
                // 上
                retVal.command = TOUCH_COMMAND_UP;
#ifdef TOUCH_DEBUG
                uart_debug_snd_buf_puts("TOUCH_COMMAND_UP\r\n");
#endif
            } else if ((TOUCH_MARGIN > (yMax - yOld))
                    && (TOUCH_MARGIN > (yOld - yMin))
                    && (TOUCH_MOVE < (xMax - xOld))
                    && (TOUCH_MARGIN > (xOld - xMin))) {
                // 左
                retVal.command = TOUCH_COMMAND_LEFT;
#ifdef TOUCH_DEBUG
                uart_debug_snd_buf_puts("TOUCH_COMMAND_LEFT\r\n");
#endif

            } else if ((TOUCH_MARGIN > (yMax - yOld))
                    && (TOUCH_MARGIN > (yOld - yMin))
                    && (TOUCH_MARGIN > (xMax - xOld))
                    && (TOUCH_MOVE < (xOld - xMin))) {
                // 右
                retVal.command = TOUCH_COMMAND_RIGHT;
#ifdef TOUCH_DEBUG
                uart_debug_snd_buf_puts("TOUCH_COMMAND_RIGHT\r\n");
#endif
            } else {
                retVal.command = TOUCH_NOT;
            }

            // 戻っているかの検知はここでできると思う
            // 後日実装？
        } else {
            retVal.command = TOUCH_NOT;
        }
    }

    return retVal;
}

static void lcd_touch_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    BaseType_t ret = 0;
    TOUCH_COMMAND retVal;

    for (;;) {
        retVal = touch_gesture();
        if (retVal.command != TOUCH_NOT) {
#if 0
            char test[30];
            sprintf(test, "send:%d:%d:%d\r\n", retVal.p.x, retVal.p.y, retVal.command);
            uart_debug_snd_buf_puts(test);
#endif
            ret = xQueueSend(touch_queue, &retVal, 0);
            if (ret == errQUEUE_FULL) {
                // キューがフルでデータが格納不可
#ifdef TOUCH_DEBUG
                uart_debug_snd_buf_puts("COMMAND_QUEUE_FULL\r\n");
#endif
            }
        }

        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);
    }
}

/* --- グローバル関数群 ------------------------------------------------------*/
void touch_initialize(void) {
    // バッファ初期化
    touch_rcv_buf.wp = 0;
    touch_rcv_buf.overFlow = 0;
    memset(touch_rcv_buf.dat, 0, TOUCH_RCV_BUF_SIZE);

    // PENIRQ端子 BUSY端子 初期化
    PENIRQ_PORT_TRIS = 1;
    BUSY_PORT_TRIS = 1;

	InitSpiTouch();

    // IC初期化のため
	touch_cs_select(0);

    SPI3BUF = 0x92; // 1 001 0 0 10 12bit 差動
    while (!SPI3STATbits.SPIRBF);
    unsigned char dummy = SPI3BUF;

    SPI3BUF = 0x00;
    while (!SPI3STATbits.SPIRBF);
    dummy = SPI3BUF;

    SPI3BUF = 0x00;
    while (!SPI3STATbits.SPIRBF);
    dummy = SPI3BUF;

	touch_cs_select(1);

    now_touch_command.command = TOUCH_NOT;
    now_touch_command.p.x = 0;
    now_touch_command.p.y = 0;

    touch_queue = xQueueCreate(QUEUE_LENGTH, sizeof (TOUCH_COMMAND));
    if (touch_queue == 0) {
        // キュー生成失敗
        return;
    }
}

TOUCH_COMMAND get_touch(void) {
    TOUCH_COMMAND command;

    command.command = TOUCH_NOT;

    xQueueReceive(touch_queue, &command, 0);
#if 0
    char test[30];
    sprintf(test, "receive:%d:%d:%d\r\n", command.p.x, command.p.y, command.command);
    uart_debug_snd_buf_puts(test);
#endif
    return command;
}

unsigned char get_touch_buf_empty(void) {
    if (uxQueueMessagesWaiting(touch_queue) <= 0) {
        // キューが空
        return 1;
    } else {
        // キューに受信データが格納されている
        return 0;
    }
}

void create_lcd_touch_task(void) {
    BaseType_t ret;

    ret = xTaskCreate(lcd_touch_task, "lcd_touch", TASK_STACK_SIZE, NULL,
            TASK_PRIORITY, NULL);
    if (ret != pdPASS) {
        // タスク生成失敗
        uart_debug_snd_buf_puts("create_lcd_touch_task failed\r\n");
    }
}

