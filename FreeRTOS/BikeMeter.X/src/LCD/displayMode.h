/*
 * File:   displayMode.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __DISPLAY_MODE_H_
#define __DISPLAY_MODE_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "lcd_touch.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
enum eMode {
    MODE_NOMAL = 0,
    MODE_GRAPH,
    MODE_GPS,
    MODE_SPEED,
    MODE_VOLT,
    MODE_CURRENT,
    MODE_PICTURE,
    MODE_DRAW,
    MODE_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/


/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void create_lcd_task(void);

#endif // __DISPLAY_MODE_H_

