/*
 * File:   displayTaskBar.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayTaskBar.h"
#include "colorlcd_libdsPICVH.h"
#include "tiny_sprintf.h"
#include "sd.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void setTaskDispTime(unsigned char forceFlg) {
    static unsigned char minOld = 0xFF;
    static unsigned char loggingStateOld = SD_STATE_MAX;

    if (forceFlg == 1 || get_gps_min() != minOld) {
        minOld = get_gps_min();

        Str_Tsprintf(_str, "%02d:%02d          ",
                get_gps_hour(), get_gps_min());
        lcd_write_str(0, 0, _str, WHITE, BLACK);
    }

    unsigned char sd_logging_state = get_sd_logging_state();
    if (forceFlg == 1 || sd_logging_state != loggingStateOld) {
        loggingStateOld = sd_logging_state;

        lcd_write_str(15, 0, (char*) sd_logging_str_list[sd_logging_state], WHITE, BLACK);
    }
}
