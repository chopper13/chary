/*
 * File:   displayMode_current.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __DISPLAY_MODE_CURRENT_H_
#define __DISPLAY_MODE_CURRENT_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "lcd_touch.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void mode_current_init(void);
extern void mode_current(TOUCH_COMMAND command);

#endif // __DISPLAY_MODE_CURRENT_H_

