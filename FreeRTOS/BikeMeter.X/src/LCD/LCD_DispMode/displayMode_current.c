/*
 * File:   displayMode_current.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_current.h"
#include "colorlcd_libdsPICVH.h"
#include "tiny_sprintf.h"
#include "displayTaskBar.h"
#include "adc.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static float curr1_old;
static float curr2_old;
static float curr3_old;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_current_init(void) {

    setTaskDispTime(1);
    mainDispClear(BLACK);
    lcd_write_str(0, 2, "CURR1[A]:", WHITE, BLACK);
    lcd_write_str(0, 3, "CURR2[A]:", WHITE, BLACK);
    lcd_write_str(0, 4, "CURR3[A]:", WHITE, BLACK);

    curr1_old = -999.9;
    curr2_old = -999.9;
    curr3_old = -999.9;
}

void mode_current(TOUCH_COMMAND command) {
    setTaskDispTime(1);

    float tmpVal = get_actual_val(AD_DATA_CURR1);
    if (curr1_old != tmpVal) {
        Str_Tsprintf(_str, "%02d.%d%d", (int) tmpVal,
                abs(((int) (tmpVal * 10)) % 10), abs(((int) (tmpVal * 100)) % 10));
        lcd_write_str(9, 2, _str, WHITE, BLACK);

        curr1_old = tmpVal;
    }

    if (curr2_old != tmpVal) {
        tmpVal = get_actual_val(AD_DATA_CURR2);
        Str_Tsprintf(_str, "%02d.%d%d", (int) tmpVal,
                abs(((int) (tmpVal * 10)) % 10), abs(((int) (tmpVal * 100)) % 10));
        lcd_write_str(9, 3, _str, WHITE, BLACK);

        curr2_old = tmpVal;
    }

    if (curr3_old != tmpVal) {
        tmpVal = get_actual_val(AD_DATA_CURR3);
        Str_Tsprintf(_str, "%02d.%d%d", (int) tmpVal,
                abs(((int) (tmpVal * 10)) % 10), abs(((int) (tmpVal * 100)) % 10));
        lcd_write_str(9, 4, _str, WHITE, BLACK);

        curr3_old = tmpVal;
    }
}
