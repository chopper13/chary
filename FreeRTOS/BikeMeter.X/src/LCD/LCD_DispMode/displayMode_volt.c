/*
 * File:   displayMode_volt.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_volt.h"
#include "colorlcd_libdsPICVH.h"
#include "JPEGImage.h"
#include "digitalMeter.h"
#include "displayTaskBar.h"
#include "adc.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

// VOLT
// 小数点描画位置
#define DECIMAL_POINT_X		155
#define DECIMAL_POINT_Y		1
#define DECIMAL_POINT_SIZE	10

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_volt_init(void) {
    setTaskDispTime(1);
    mainDispClear(BLACK);
    lcd_write_seven_seg(-1, SEG_VOLT_100);
    lcd_write_seven_seg(-1, SEG_VOLT_10);
    lcd_write_seven_seg(-1, SEG_VOLT_1);

    // 小数点描画
    int i;
    for (i = 0; i < DECIMAL_POINT_SIZE; i++) {
        lcd_write_line(DECIMAL_POINT_X, DECIMAL_POINT_Y + i,
                DECIMAL_POINT_X + DECIMAL_POINT_SIZE, DECIMAL_POINT_Y + i, WHITE);
    }

    JPEGPutImage(279, 189, &jpegVolt);
}

void mode_volt(TOUCH_COMMAND command) {
    setTaskDispTime(1);

    float tmpVal = get_actual_val(AD_DATA_VOLT);
    lcd_write_seven_seg((char) ((int) (tmpVal / 10) % 10), SEG_VOLT_100);
    lcd_write_seven_seg((char) ((int) tmpVal % 10), SEG_VOLT_10);
    lcd_write_seven_seg((char) ((int) (tmpVal * 10) % 10), SEG_VOLT_1);
}
