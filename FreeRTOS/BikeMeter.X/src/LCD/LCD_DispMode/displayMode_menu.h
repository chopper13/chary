/*
 * File:   displayMode_menu.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __DISPLAY_MODE_MENU_H_
#define __DISPLAY_MODE_MENU_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void mode_menu_str_init(void);
extern void print_mode_menu_init(void);
extern unsigned char mode_menu(TOUCH_COMMAND command);
extern unsigned char get_change_menu(void);

#endif // __DISPLAY_MODE_MENU_H_

