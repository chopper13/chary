/*
 * File:   displayMode_speed.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_speed.h"
#include "colorlcd_libdsPICVH.h"
#include "JPEGImage.h"
#include "digitalMeter.h"
#include "displayTaskBar.h"
#include "gps.h"
#include "port.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_speed_init(void) {
    setTaskDispTime(1);
    mainDispClear(BLACK);
    lcd_write_seven_seg(-1, SEG_SPEED_100);
    lcd_write_seven_seg(-1, SEG_SPEED_10);
    lcd_write_seven_seg(-1, SEG_SPEED_1);

    lcd_write_seven_seg(-1, SEG_REV_10000);
    lcd_write_seven_seg(-1, SEG_REV_1000);
    lcd_write_seven_seg(-1, SEG_REV_100);
    lcd_write_seven_seg(-1, SEG_REV_10);
    lcd_write_seven_seg(-1, SEG_REV_1);

    JPEGPutImage(239, 90, &jpegSpeed);
    JPEGPutImage(239, 194, &jpegRpm);
}

void mode_speed(TOUCH_COMMAND command) {
    setTaskDispTime(1);

    unsigned int speed = (unsigned int) get_gps_speed();
    char mod = 0;
    if (speed < 100) {
        mod = 10;
    } else {
        mod = (char) ((speed / 100) % 10);
    }
    lcd_write_seven_seg(mod, SEG_SPEED_100);

    if (speed < 10) {
        mod = 10;
    } else {
        mod = (char) ((speed / 10) % 10);
    }
    lcd_write_seven_seg(mod, SEG_SPEED_10);
    lcd_write_seven_seg((char) (speed % 10), SEG_SPEED_1);

    unsigned short rev = get_rpm();
    if (rev < 10000) {
        mod = 10;
    } else {
        mod = 10;
        mod = (char) ((rev / 10000) % 10);
    }
    lcd_write_seven_seg(mod, SEG_REV_10000);

    if (rev < 1000) {
        mod = 10;
    } else {
        mod = 10;
        mod = (char) ((rev / 1000) % 10);
    }
    lcd_write_seven_seg(mod, SEG_REV_1000);

    if (rev < 100) {
        mod = 10;
    } else {
        mod = (char) ((rev / 100) % 10);
    }
    lcd_write_seven_seg(mod, SEG_REV_100);

    if (rev < 10) {
        mod = 10;
    } else {
        mod = (char) ((rev / 10) % 10);
    }
    lcd_write_seven_seg(mod, SEG_REV_10);

    lcd_write_seven_seg((char) (rev % 10), SEG_REV_1);
}
