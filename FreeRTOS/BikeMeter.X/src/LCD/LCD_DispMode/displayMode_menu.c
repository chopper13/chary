/*
 * File:   displayMode_current.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_current.h"
#include "colorlcd_libdsPICVH.h"
#include "displayTaskBar.h"
#include "displayMode.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

// メニュー表示 文字・背景色
#define DEFAULT_FONT_COLOR	WHITE
#define DEFAULT_BACK_COLOR	BLACK
#define MENU_FONT_COLOR		BLACK
#define MENU_BACK_COLOR		WHITE

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

static volatile unsigned char change_menu;
static unsigned char menu;
TOUCH_COMMAND touch_old;

static unsigned char menu_str_max;


/* --- テーブル定義 ----------------------------------------------------------*/
static const char *menu_str_list[MODE_MAX] = {
    "nomal  ",
    "graph  ",
    "gps    ",
    "speed  ",
    "volt   ",
    "current",
    "picture",
    "draw   ",
};

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static void print_mode_menu(unsigned char menu) {
    unsigned char i;
    for (i = 0; i < MODE_MAX; i++) {
        unsigned short fontColor;
        unsigned short backColor;

        if (i == menu) {
            // 選択メニューのみ文字色変更
            fontColor = MENU_FONT_COLOR;
            backColor = MENU_BACK_COLOR;
        } else {
            // それ以外はデフォルトカラーとする
            fontColor = DEFAULT_FONT_COLOR;
            backColor = DEFAULT_BACK_COLOR;
        }

        // メニュー表示
        lcd_write_str(0, i, (char*) menu_str_list[i], fontColor, backColor);
    }
}

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_menu_str_init(void) {
    // メニューの文字列最大数を算出
    menu_str_max = 0;
    int i;
    for (i = 0; i < MODE_MAX; i++) {
        int len = 0;
        while (menu_str_list[i][len] != '\0') {
            len++;
        }

        if (menu_str_max < len) {
            menu_str_max = len;
        }
    }
}

unsigned char mode_menu(TOUCH_COMMAND command) {
    unsigned char retVal = MODE_MAX;

    if (command.command != TOUCH_NOW
            && touch_old.command == TOUCH_NOW) {
        if (menu != MODE_MAX) {
            retVal = menu;
        } else {
            retVal = MODE_MAX;
        }
    } else {
        unsigned char tmpMenu = MODE_MAX;

        // どのメニューが選択されているか判定
        unsigned char i;
        for (i = 0; i < MODE_MAX; i++) {
            if (0 < command.p.x && command.p.x <= menu_str_max * 16
                    && ENDROW - ((i + 1) * 16) < command.p.y
                    && command.p.y <= ENDROW - (i * 16)) {
                tmpMenu = i;
            }
        }

        // 選択されているメニューが変化したので再表示
        if (tmpMenu != MODE_MAX
                && tmpMenu != menu) {
            print_mode_menu(tmpMenu);
            menu = tmpMenu;
        }

        // モードが決定していない状態なので、MODE_MAXを返す
        retVal = MODE_MAX;
    }

    touch_old = command;
    return retVal;
}

void print_mode_menu_init(void) {
    lcd_write_clear(BLACK);

    // メニュー未選択状態にする
    menu = MODE_MAX;
    print_mode_menu(menu);
}

unsigned char get_change_menu(void) {
    return change_menu;
}

