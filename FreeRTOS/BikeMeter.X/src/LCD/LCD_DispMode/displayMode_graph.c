/*
 * File:   displayMode_graph.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_graph.h"
#include "FreeRTOS.h"
#include "colorlcd_libdsPICVH.h"
#include "displayGraph.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
enum eGraphbutton {
    GRAPH_BTN_OILTEMP = 0,
    GRAPH_BTN_VOLT,
    GRAPH_BTN_AIRTEMP,
    GRAPH_BTN_CURR1,
    GRAPH_BTN_ACCURR,
    GRAPH_BTN_CURR2,
    GRAPH_BTN_CURR3,
    GRAPH_BTN_INT,
    GRAPH_BTN_MAX,
};

enum eGraphInterval {
    GRAPH_INT_5 = 0,
    GRAPH_INT_10,
    GRAPH_INT_20,
    GRAPH_INT_50,
    GRAPH_INT_100,
    GRAPH_INT_MAX,
};

enum ePoint {
    POINT_START = 0,
    POINT_END,
    POINT_MAX,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static TOUCH_COMMAND touch_old;
static unsigned char graph_btn_chooseing;
static unsigned char graph_show_flg[AD_DATA_MAX];
static unsigned char graph_interval_idx;
static TickType_t graph_draw_time_old;

/* --- テーブル定義 ----------------------------------------------------------*/
static const TOUCH_POINT graph_show_button_area[GRAPH_BTN_MAX][POINT_MAX] = {
    {
        {0, 16},
        {79, 31},
    },
    {
        {80, 16},
        {159, 31},
    },
    {
        {160, 16},
        {239, 31},
    },
    {
        {0, 0},
        {79, 15},
    },
    {
        {80, 0},
        {159, 15},
    },
    {
        {160, 0},
        {239, 15},
    },
    {
        {240, 0},
        {319, 15},
    },
    {
        {240, 16},
        {319, 31},
    },
};

static const TOUCH_POINT graph_show_button[AD_DATA_MAX] = {
    {0, 13},
    {5, 13},
    {10, 13},
    {0, 14},
    {5, 14},
    {10, 14},
    {15, 14},
};

// 1tick = 1kHz = 1msec
static const unsigned int graph_interval_list[GRAPH_INT_MAX] = {
    5,
    10,
    20,
    50,
    100,
};

static const char *graph_interval_str_list[GRAPH_INT_MAX] = {
    "5   ",
    "10  ",
    "20  ",
    "50  ",
    "100 ",
};

static const TOUCH_POINT graph_interval_button = {15, 13};

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static void graph_bottom(char select) {
    int i;
    for (i = 0; i < AD_DATA_MAX; i++) {
        if (select == i) {
            lcd_write_str(graph_show_button[i].x, graph_show_button[i].y,
                    (char*) adc_name_list[i], graph_line_color[i], WHITE);
        } else {
            if (graph_show_flg[i] == 1) {
                lcd_write_str(graph_show_button[i].x, graph_show_button[i].y,
                        (char*) adc_name_list[i], graph_line_color[i], BLACK);
            } else {
                lcd_write_str(graph_show_button[i].x, graph_show_button[i].y,
                        (char*) adc_name_list[i], GLAY, BLACK);
            }
        }
    }
}

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_graph_init(void) {
    int i;

    for (i = 0; i < AD_DATA_MAX; i++) {
        graph_show_flg[i] = 1;
    }
    graph_interval_idx = GRAPH_INT_5;

    lcd_write_clear(BLACK);

    Display_Graph_SetShow(graph_show_flg);

    graph_btn_chooseing = GRAPH_BTN_MAX;

    graph_bottom(-1);
    lcd_write_str(graph_interval_button.x, graph_interval_button.y,
            (char*) graph_interval_str_list[graph_interval_idx], WHITE, BLACK);

    //Display_Graph_Reset();
    graph_draw_time_old = xTaskGetTickCount();
}

void mode_graph(TOUCH_COMMAND command) {
    float graph_point[AD_DATA_MAX];
    int i;
#if 1
    // グラフ描画
    if (graph_interval_list[graph_interval_idx]
            < (unsigned int) (xTaskGetTickCount() - graph_draw_time_old)) {
        for (i = 0; i < AD_DATA_MAX; i++) {
            graph_point[i] = (float) get_ave_val(i) / 0x3FF;
        }
        Display_Graph_Plot(graph_point);

        graph_draw_time_old = xTaskGetTickCount();
    }


    // ラインの表示・非表示、周期の変更
    if (command.command != TOUCH_NOW
            && touch_old.command == TOUCH_NOW) {
        // 選択確定

        switch (graph_btn_chooseing) {
            case GRAPH_BTN_OILTEMP:
            case GRAPH_BTN_VOLT:
            case GRAPH_BTN_AIRTEMP:
            case GRAPH_BTN_CURR1:
            case GRAPH_BTN_ACCURR:
            case GRAPH_BTN_CURR2:
            case GRAPH_BTN_CURR3:
                if (graph_show_flg[graph_btn_chooseing] == 1) {
                    graph_show_flg[graph_btn_chooseing] = 0;
                } else {
                    graph_show_flg[graph_btn_chooseing] = 1;
                }
                Display_Graph_SetShow(graph_show_flg);

                graph_bottom(-1);
                break;

            case GRAPH_BTN_INT:
                graph_interval_idx++;
                if (GRAPH_INT_MAX <= graph_interval_idx) {
                    graph_interval_idx = GRAPH_INT_5;
                }

                lcd_write_str(graph_interval_button.x, graph_interval_button.y,
                        (char*) graph_interval_str_list[graph_interval_idx], WHITE, BLACK);
                break;

            default:
                break;
        }

        graph_btn_chooseing = GRAPH_BTN_MAX;
    } else if (command.command == TOUCH_NOW) {
        // 選択中

        // ラインの表示・非表示、周期の変更
        for (i = 0; i < GRAPH_BTN_MAX; i++) {
            if (graph_show_button_area[i][POINT_START].x <= command.p.x
                    && command.p.x <= graph_show_button_area[i][POINT_END].x
                    && graph_show_button_area[i][POINT_START].y <= command.p.y
                    && command.p.y <= graph_show_button_area[i][POINT_END].y) {
                if (graph_btn_chooseing != i) {
                    graph_btn_chooseing = i;

                    switch (i) {
                        case GRAPH_BTN_OILTEMP:
                        case GRAPH_BTN_VOLT:
                        case GRAPH_BTN_AIRTEMP:
                        case GRAPH_BTN_CURR1:
                        case GRAPH_BTN_ACCURR:
                        case GRAPH_BTN_CURR2:
                        case GRAPH_BTN_CURR3:
                            graph_bottom(i);
                            lcd_write_str(graph_interval_button.x, graph_interval_button.y,
                                    (char*) graph_interval_str_list[graph_interval_idx], WHITE, BLACK);
                            break;

                        case GRAPH_BTN_INT:
                            graph_bottom(-1);
                            lcd_write_str(graph_interval_button.x, graph_interval_button.y,
                                    (char*) graph_interval_str_list[graph_interval_idx], BLACK, WHITE);
                            break;

                        default:
                            break;

                    }
                }
            }
        }
    }
    touch_old = command;
#endif
}
