/*
 * File:   displayMode_nomal.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_nomal.h"
#include "colorlcd_libdsPICVH.h"
#include "tiny_sprintf.h"
#include "displayTaskBar.h"
#include "adc.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static float airtemp_old;
static float volt_old;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_nomal_init(void) {
    setTaskDispTime(1);
    mainDispClear(BLACK);
    lcd_write_str(0, 2, "Temp:", WHITE, BLACK);
    lcd_write_str(0, 3, "Volt:", WHITE, BLACK);

    airtemp_old = -999.9;
    volt_old = -999.9;
}

void mode_nomal(TOUCH_COMMAND command) {
    setTaskDispTime(1);

    int intVal;
    float tmpVal = get_actual_val(AD_DATA_AIRTEMP);
    if (airtemp_old != tmpVal) {
        intVal = (int) tmpVal;
        Str_Tsprintf(_str, "%02d.%d", intVal, abs((int) ((tmpVal - intVal) * 10)));
        lcd_write_str(5, 2, _str, WHITE, BLACK);

        airtemp_old = tmpVal;
    }

    tmpVal = get_actual_val(AD_DATA_VOLT);
    if (volt_old != tmpVal) {
        intVal = (int) tmpVal;
        Str_Tsprintf(_str, "%02d.%d", intVal, abs((int) ((tmpVal - intVal) * 10)));
        lcd_write_str(5, 3, _str, WHITE, BLACK);

        volt_old = tmpVal;
    }
    
     Str_Tsprintf(_str, "%d", get_rpm());
       lcd_write_str(5, 4, _str, WHITE, BLACK);
}
