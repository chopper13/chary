/*
 * File:   displayMode_draw.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_draw.h"
#include "colorlcd_libdsPICVH.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define DEBUG_DRAW_POINT

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static unsigned char touch_start_flg;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_draw_init(void) {
    lcd_write_clear(BLACK);
    touch_start_flg = 1;
}

void mode_draw(TOUCH_COMMAND command) {
    static TOUCH_COMMAND touch_old;

    if (command.command != TOUCH_NOT) {
#ifdef DEBUG_DRAW_POINT
        char test[30];
        Str_Vtsprintf(test, "x:%d y;%d\r\n", command.p.x, command.p.y);
        uart_debug_snd_buf_puts(test);
#endif
        // 角ポイントの場合は異常発生の可能性が高いので、
        // 描画対象としない
        if (command.p.x == 0 && command.p.y == 0) {
            return;
        }
        if (command.p.x == ENDCOL && command.p.y == ENDROW) {
            return;
        }

        if (touch_start_flg == 1) {
            touch_start_flg = 0;
            touch_old.p.x = command.p.x;
            touch_old.p.y = command.p.y;
        }

        lcd_write_line(command.p.x, command.p.y, touch_old.p.x, touch_old.p.y, WHITE);

        touch_old.p.x = command.p.x;
        touch_old.p.y = command.p.y;
    } else {
    	touch_start_flg = 1;
    }
}
