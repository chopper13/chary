/*
 * File:   displayTaskBar.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __DISPLAY_TASK_H_
#define __DISPLAY_TASK_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define TASK_DISP_POINT 0
#define TASK_DISP_SIZE 15

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void setTaskDispTime(unsigned char forceFlg);

#endif // __DISPLAY_TASK_H_

