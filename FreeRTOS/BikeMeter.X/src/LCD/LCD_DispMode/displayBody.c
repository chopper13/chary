/*
 * File:   displayBody.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayBody.h"
#include "colorlcd_libdsPICVH.h"
#include "displayTaskBar.h"


/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define MAIN_DISP_POINT TASK_DISP_SIZE
#define MAIN_DISP_SIZE ENDROW - MAIN_DISP_POINT

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mainDispClear(unsigned short Color) {
    int i, j;

    Glcd_out(0x0020, MAIN_DISP_POINT);
    Glcd_out(0x0021, 0);
    for (j = MAIN_DISP_POINT; j < ENDROW; j++) { // Y軸全部
        for (i = 0; i < ENDCOL; i++) { // X軸全部
            Glcd_out(0x0022, Color); // 同色で埋める
        }
    }
}
