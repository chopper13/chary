/*
 * File:   displayMode_gps.c
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode_gps.h"
#include "colorlcd_libdsPICVH.h"
#include "lcd_touch.h"
#include "tiny_sprintf.h"
#include "displayTaskBar.h"
#include "gps.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static int lat_degree_old;
static int lat_min_old;
static int lon_degree_old;
static int lon_min_old;
static float declination_old;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/
void mode_gps_init(void) {

    setTaskDispTime(1);
    mainDispClear(BLACK);
    lcd_write_str(0, 2, "LAT:", WHITE, BLACK);
    lcd_write_str(0, 3, "LGN:", WHITE, BLACK);
    lcd_write_str(0, 5, "COURSE:", WHITE, BLACK);

    lat_degree_old = -999;
    lat_min_old = -999;
    lon_degree_old = -999;
    lon_min_old = -999;
    declination_old = -9999.9;
}

void mode_gps(TOUCH_COMMAND command) {
    setTaskDispTime(1);

    int degree = get_gps_lat_degree();
    int min = get_gps_lat_min();

    if (lat_degree_old != degree && lat_min_old != min) {
        Str_Tsprintf(_str, "%03d.%02d", get_gps_lat_degree(), get_gps_lat_min());
        lcd_write_str(4, 2, _str, WHITE, BLACK);

        lat_degree_old = degree;
        lat_min_old = min;
    }

    degree = get_gps_lon_degree();
    min = get_gps_lon_min();
    if (lon_degree_old != degree && lon_min_old != min) {
        Str_Tsprintf(_str, "%03d.%02d", get_gps_lon_degree(), get_gps_lon_min());
        lcd_write_str(4, 3, _str, WHITE, BLACK);

        lon_degree_old = degree;
        lon_min_old = min;
    }
#if 0
    float declination = get_gps_declination();
    if (declination_old != declination) {
        Str_Tsprintf(_str, "%03d.%d", (int) declination, abs((int) ((declination - intVal) * 10)));
        lcd_write_str(7, 5, _str, WHITE, BLACK);

        declination_old = declination;
    }
#endif
}


