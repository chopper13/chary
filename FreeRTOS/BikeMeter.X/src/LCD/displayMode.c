/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     displayMode.c
 *
 *     ディスプレイ アプリ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include "displayMode.h"
#include "FreeRTOS.h"
#include "task.h"
#include "colorlcd_libdsPICVH.h"
#include "timer.h"
#include "tiny_sprintf.h"
#include "func.h"
#include "displayTaskBar.h"

#include "displayMode_current.h"
#include "displayMode_draw.h"
#include "displayMode_gps.h"
#include "displayMode_graph.h"
#include "displayMode_nomal.h"
#include "displayMode_speed.h"
#include "displayMode_volt.h"
#include "displayMode_picture.h"
#include "displayMode_menu.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define TOUCH_DEBUG

// MEMU
// メニュー初期値
#define START_MODE	MODE_SPEED

#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		1		// タスク優先度
#define TASK_STACK_SIZE		1000	// スタックサイズ

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
static char menu;

/* --- テーブル定義 ----------------------------------------------------------*/


static void (*disp_mode_init[])() = {
    mode_nomal_init,
    mode_graph_init,
    mode_gps_init,
    mode_speed_init,
    mode_volt_init,
    mode_current_init,
    mode_picture_init,
    mode_draw_init,
};

static void (*disp_mode_runing[])(TOUCH_COMMAND) = {
    mode_nomal,
    mode_graph,
    mode_gps,
    mode_speed,
    mode_volt,
    mode_current,
    mode_picture,
    mode_draw,
};


/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static void MainDispFunc(void) {

    static unsigned char menu_state = 0;
    static unsigned char mode = START_MODE;
    BaseType_t ret = 0;
    TOUCH_COMMAND touch;
    char touch_get_cnt = 0;
    char touch_get_cnt_old = -1;

    touch.p.x = 0;
    touch.p.y = 0;
    touch.command = TOUCH_NOT;

    while (touch_get_cnt_old != touch_get_cnt) {

        // タッチスクリーン取得
        if (!get_touch_buf_empty()) {
            touch = get_touch();
            touch_get_cnt++;
        }

        if (touch_get_cnt_old != touch_get_cnt) {
            // 初回かタッチがある場合のみ処理を行う

            if (menu_state == 1) {
                // メニュー表示中

                unsigned char ret_menu = mode_menu(touch);
                if (ret_menu != MODE_MAX) {
                    // モードが確定した場合は各モード画面に遷移
                    mode = ret_menu;
                    menu_state = 0;

                    // 各モード画面で初期化
                    if (MODE_NOMAL <= mode && mode < MODE_MAX) {
                        disp_mode_init[mode]();
                        disp_mode_runing[mode](touch);
                    } else {
                        lcd_write_clear(BLACK);
                        lcd_write_str(0, 1, "ERROR!!!", RED, BLACK);
                    }
                }
            } else {
                // 各画面表示中

                if (touch.command == TOUCH_COMMAND_LEFT) {
                    // メニュー表示に遷移
                    menu_state = 1;
                    print_mode_menu_init();
                } else {
                    // 各画面更新処理
                    if (MODE_NOMAL <= mode && mode < MODE_MAX) {
                        disp_mode_runing[mode](touch);
                    } else {
                        lcd_write_clear(BLACK);
                        lcd_write_str(0, 1, "ERROR!!!", RED, BLACK);
                    }

                }
            }
        }

        touch_get_cnt_old = touch_get_cnt;
    }
}

static void mainDispInit(void) {
    menu = START_MODE;

    mode_menu_str_init();

    // 初期画面表示
    if (MODE_NOMAL <= menu && menu < MODE_MAX) {
        disp_mode_init[menu]();
    } else {
        lcd_write_clear(BLACK);
        lcd_write_str(0, 1, "ERROR!!!", RED, BLACK);
    }

}

static void lcd_mode_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();

    mainDispInit();

    for (;;) {
        MainDispFunc();

        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);
    }
}

/* --- グローバル関数群 ------------------------------------------------------*/
void create_lcd_task(void) {
    BaseType_t ret;

    ret = xTaskCreate(lcd_mode_task, "lcd_mode", TASK_STACK_SIZE, NULL,
            TASK_PRIORITY, NULL);
    if (ret != pdPASS) {
        // タスク生成失敗
        uart_debug_snd_buf_puts("create_lcd_task failed\r\n");
    }
}
