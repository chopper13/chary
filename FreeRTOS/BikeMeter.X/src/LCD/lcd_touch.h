/*
 * File:   lcd_touch.h
 * Author: ei
 *
 * Created on 2013/11/06, 23:29
 */

#ifndef __LCD_TOUCH_H_
#define __LCD_TOUCH_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/
enum {
    // タッチコマンド
    TOUCH_COMMAND_UP,
    TOUCH_COMMAND_DOWN,
    TOUCH_COMMAND_RIGHT,
    TOUCH_COMMAND_LEFT,
    TOUCH_COMMAND_NONE,
    TOUCH_NOW,
    TOUCH_NOT,
};

/* --- 構造体・共用体定義 ----------------------------------------------------*/
typedef struct _TOUCH_POINT {
    unsigned short x;
    unsigned short y;
} TOUCH_POINT;

typedef struct _TOUCH_COMMAND {
    TOUCH_POINT p;
    unsigned char command;
} TOUCH_COMMAND;

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void touch_initialize(void);
extern TOUCH_COMMAND get_touch(void);
extern unsigned char get_touch_buf_empty(void);
extern void create_lcd_touch_task(void);

#endif // __LCD_TOUCH_H_

