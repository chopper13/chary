/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//
//	ファイル名 : tiny_sprintf.c
//
//	機能	   : 簡易 sprintf ソース
//
//
//	履歴	   : 2013.04.09 新規作成
//
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
//#include <string.h>

#include "tiny_sprintf.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
char _str[64];

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
int Str_Vtsprintf(char* buff, char* fmt, va_list arg);
int Str_TsprintfDecimal(signed long val, char* buff, int zf, int wd);
int Str_TsprintfUnsignedDecimall(unsigned long val, char* buff, int zf, int wd);
int Str_TsprintfHex(unsigned long val, char* buff, char c, int zf, int wd);

/* --- グローバル関数群 ------------------------------------------------------*/

/**
 * @brief	メッセージ生成
 *
 *	変換形式に対応した形式に変換する。
 *	実装変換形式は、%dと%lのみとする。
 *
 * @param	buff	生成メッセージ格納
 * @param	fmt		変換形式
 * @param	arg		変換値
 *
 * @retval	変換文字数
 */
int Str_Vtsprintf(char* buff, char* fmt, va_list arg) {
    int len;
    int size;
    int zeroflag, width;

    size = 0;
    len = 0;

    while (*fmt) {
        if (*fmt == '%') {
            // % に関する処理
            zeroflag = width = 0;
            fmt++;

            if (*fmt == '0') {
                fmt++;
                zeroflag = 1;
            }
            if ((*fmt >= '0') && (*fmt <= '9')) {
                width = *(fmt++) - '0';
            }

            // TODO: 各変換で落ちてるっぽい
            switch (*fmt) {
                case 'd': // 10進数
                    size = Str_TsprintfDecimal(va_arg(arg, signed long),
                            buff, zeroflag, width);
                    break;
                case 'l':
                    size = Str_TsprintfUnsignedDecimall(va_arg(arg, unsigned long),
                            buff, zeroflag, width);
                    break;
                case 'x': // 16進数 0-f
                    size = Str_TsprintfHex(va_arg(arg, unsigned long),
                            buff, 'a', zeroflag, width);
                    break;
                case 'X': // 16進数 0-F
                    size = Str_TsprintfHex(va_arg(arg, unsigned long),
                            buff, 'A', zeroflag, width);
                    break;
#if 0
                case 'c': // キャラクター
                    size = Str_TsprintfChar(va_arg(arg, char),
                            buff, zeroflag, width);
                    break;
                case 's': // ASCII文字列
                    size = Str_TsprintfString(va_arg(arg, const char*),
                            buff, zeroflag, width);
                    break;
#endif
                default: // コントロールコード以外の文字
                    // %%(%に対応)はここで対応される
                    len++;
                    *(buff++) = *fmt;
                    break;
            }
            len += size;
            buff += size;
            fmt++;
        } else {
            *(buff++) = *(fmt++);
            len++;
        }
    }

    *buff = '\0'; // 終端を入れる

    va_end(arg);
    return (len);
}

/**
 * @brief	符号有り10進数文字列生成
 *
 * @param	val		変換値
 * @param	buff	生成文字列格納
 * @param	zf		0埋め あり・なし
 * @param	wd		0埋め文字数
 *
 * @retval	変換文字数
 */
int Str_TsprintfDecimal(signed long val, char* buff, int zf, int wd) {
    int i = 0;
    char tmp[12] = {0};
    char* ptmp = &tmp[11];
    int len = 0;
    int minus = 0;

    if (!val) { // 指定値が0の場合
        *(ptmp--) = '0';
        len++;
    } else {
        // マイナスの値の場合には2の補数を取る
        if (val < 0) {
            val = ~val;
            val++;
            minus = 1;
        }
        while (val) {
            // バッファアンダーフロー対策
            if (len >= 10) {
                break;
            }

            *ptmp = (val % 10) + '0';
            val /= 10;
            ptmp--;
            len++;
        }
    }

    // 符号、桁合わせに関する処理
    if (zf) {
        if (minus) {
            wd--;
        }
        while (len < wd) {
            *(ptmp--) = '0';
            len++;
        }
        if (minus) {
            *(ptmp--) = '-';
            len++;
        }
    } else {
        if (minus) {
            *(ptmp--) = '-';
            len++;
        }
        while (len < wd) {
            *(ptmp--) = ' ';
            len++;
        }
    }

    // 生成文字列のバッファコピー
    for (i = 0; i < len; i++) {
        *(buff++) = *(++ptmp);
    }

    return (len);
}

/**
 * @brief	符号無し10進数文字列生成
 *
 * @param	val		変換値
 * @param	buff	生成文字列格納
 * @param	zf		0埋め あり・なし
 * @param	wd		0埋め文字数
 *
 * @retval	変換文字数
 */
int Str_TsprintfUnsignedDecimall(unsigned long val, char* buff, int zf, int wd) {
    int i = 0;
    char tmp[12] = {0};
    char* ptmp = &tmp[11];
    int len = 0;

    if (!val) { // 指定値が0の場合
        *(ptmp--) = '0';
        len++;
    } else {
        while (val) {
            // バッファアンダーフロー対策
            if (len >= 10) {
                break;
            }

            *ptmp = (val % 10) + '0';
            val /= 10;
            ptmp--;
            len++;
        }
    }

    // 符号、桁合わせに関する処理
    if (zf) {
        while (len < wd) {
            *(ptmp--) = '0';
            len++;
        }
    } else {
        while (len < wd) {
            *(ptmp--) = ' ';
            len++;
        }
    }

    // 生成文字列のバッファコピー
    for (i = 0; i < len; i++) {
        *(buff++) = *(++ptmp);
    }

    return (len);
}

/**
 * @brief	16進数文字列生成
 *
 * @param	val		変換値
 * @param	buff	生成文字列格納
 * @param	c		16進数出力基準文字
 * @param	zf		0埋め あり・なし
 * @param	wd		0埋め文字数
 *
 * @retval	文字数
 */
#if 1

int Str_TsprintfHex(unsigned long val, char* buff, char c, int zf, int wd) {
    unsigned long i = 0;
    unsigned char flag = 0;
    unsigned char data;
    int buff_index = 0;
    char zfdata;

    if (zf != 0) {
        zfdata = '0';
    } else {
        zfdata = ' ';
    }

    if (wd > 8) do {
            buff[buff_index++] = zfdata;
        } while (--wd > 8);

    for (i = 0; i < 8; i++) {
        data = (val >> ((7 - i) * 4)) & 0x0F;

        if ((data != 0) || (i >= 7)) {
            flag = 1;
        }

        if (flag != 0) {
            if (data >= 10) {
                buff[buff_index++] = (char) (data - 10) + c;
            } else {
                buff[buff_index++] = (char) (data) + '0';
            }
        } else {
            if ((int) i >= (8 - wd)) {
                buff[buff_index++] = zfdata;
            }
        }
    }
    return buff_index;
}
#endif

/**
 * @brief	キャラクタ文字生成
 *
 * @param	val		変換値
 * @param	buff	生成文字列格納
 * @param	zf		0埋め あり・なし
 * @param	wd		0埋め文字数
 *
 * @retval	文字数
 */
#if 0

int Str_TsprintfChar(char val, char* buff, int zf, int wd) {
    int buff_index = 0;
    char zfdata;

    if (zf != 0) {
        zfdata = '0';
    } else {
        zfdata = ' ';
    }

    if (wd > 1) do {
            buff[buff_index++] = zfdata;
        } while (--wd > 1);

    buff[buff_index++] = val;

    return buff_index;
}
#endif

/**
 * @brief	文字列生成
 *
 * @param	val		変換値
 * @param	buff	生成文字列格納
 * @param	zf		0埋め あり・なし
 * @param	wd		0埋め文字数
 *
 * @retval	文字数
 */
#if 0

int Str_TsprintfString(const char* val, char* buff, int zf, int wd) {
    unsigned int buff_index = 0;
    char zfdata;
    int size;

    if (zf != 0) {
        zfdata = '0';
    } else {
        zfdata = ' ';
    }
    size = strlen(val);

    if (wd > size) do {
            buff[buff_index++] = zfdata;
        } while (--wd > size);

    // 文字列コピー
    memcpy(buff, val, size);

    return buff_index + size;
}
#endif

/**
 * @brief	簡易sprintf関数
 *
 *	変換形式に対応した形式に変換<br>
 *	sprnitf()の簡易版として、%dと%lのみ実装している。
 *
 * @param	buff	生成メッセージ格納
 * @param	fmt		変換形式
 * @param	...		変換値
 *
 * @retval	変換文字数
 */
int Str_Tsprintf(char* buff, char* fmt, ...) {
    va_list arg;
    int len = 0;

    va_start(arg, fmt);

    // 文字列を形成する
    len = Str_Vtsprintf(buff, fmt, arg);

    va_end(arg);

    return (len);
}


