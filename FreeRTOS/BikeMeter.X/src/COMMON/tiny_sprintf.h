/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */
//
//	ファイル名 : tiny_sprintf.h
//
//	機能	   : 簡易 sprintf ヘッダ
//
//
//	履歴	   : 2013.04.09 新規作成
//
/*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef __TINY_SPRINTF_H_
#define __TINY_SPRINTF_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <stdarg.h>

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
extern char _str[64];

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern int Str_Tsprintf(char* buff, char* fmt, ...);

#endif	// __TINY_SPRINTF_H_
