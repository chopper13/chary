/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     timer.c
 *
 *     タイマ関連
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include "timer.h"


/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/
unsigned long time_delta_counter; // 1ms周期割込みによりカウントアップ

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
void __attribute__((interrupt(IPL0AUTO), vector(_TIMER_3_VECTOR))) vTimer3InterruptWrapper(void);

/* --- ローカル関数群 --------------------------------------------------------*/

/* --- グローバル関数群 ------------------------------------------------------*/

/*
 *  フリーランタイマデルタカウンタ(timer3)開始
 */
void start_freerun_timer3(void) {
    T3CONbits.ON = 0x1; //Start Timer
    return;
} // start_freerun_timer3

/*
 *  フリーランタイマ 停止
 */
void stop_freerun_timer3(void) {
    T3CONbits.ON = 0x0; //Stop Timer
    return;
} // stop_freerun_timer3

/*
 *  フリーランタイマ 初期化
 */
void freerun_timer3_initialize(void) {
    T3CONSET = 0x00; // リセット

    //internal clock
    TMR3 = 0; //Clear timer register

    //prescaler=1:64
    T3CON = 0x0060;

    // 40MHz / 64 = 625kHz
    // 1sec / 625kHz = 1.6usec
    // 1.6usec * 0x0271(625)cnt = 1msec
    PR3 = 0x0271;

    IPC3bits.T3IP = 0x2; // Set priority level=1 and
    IPC3bits.T3IS = 0x1; // Set subpriority level=1

    IFS0bits.T3IF = 0;
    IEC0bits.T3IE = 1;

    // タイマカウンタ初期化
    time_delta_counter = 0;

    start_freerun_timer3();

    return;
} // freerun_timer3_initialize

/*
 *  フリーランタイマデルタカウンタ(timer2)開始
 */
void start_freerun_timer2(void) {
    T2CONbits.ON = 0x1; //Start Timer
    return;
} // start_freerun_timer2

/*
 *  フリーランタイマ 停止
 */
void stop_freerun_timer2(void) {
    T2CONbits.ON = 0x0; //Stop Timer
    return;
} // stop_freerun_timer2

/*
 *  フリーランタイマ 初期化
 */
void freerun_timer2_initialize(void) {
    T2CONSET = 0x00; // リセット

    //prescaler=1:256
    T2CON = 0x0070;

    //internal clock
    TMR2 = 0; //Clear timer register

    PR2 = 0xFFFF;

    start_freerun_timer2();

    return;
} // freerun_timer2_initialize

void vTimer3InterruptHandler(void) {
    if (IFS0bits.T3IF) {
        IFS0bits.T3IF = 0;

        // フリーランタイマデルタカウンタ加算
        time_delta_counter++;
    }

    return;
}

