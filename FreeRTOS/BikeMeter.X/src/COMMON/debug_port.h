/*
 * File:   debug_port.h
 * Author: ei
 *
 * Created on 2015/11/12, 1:10
 */

#ifndef __DEBUG_PORT_H_
#define __DEBUG_PORT_H_


#define DEBUG_PORT1_TRIS	TRISFbits.TRISF0
#define DEBUG_PORT1			LATFbits.LATF0
#define DEBUG_PORT2_TRIS	TRISFbits.TRISF1
#define DEBUG_PORT2			LATFbits.LATF1

#endif	/* __DEBUG_PORT_H_ */
