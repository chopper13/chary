/* 
 * File:   main.c
 * Author: ei
 *
 * Created on September 24, 2015, 9:10 PM
 */

//#include <stdio.h>
//#include <stdlib.h>
#include <xc.h>

#include "debug_port.h"
#include "FreeRTOS.h"
#include "task.h"

#include "pic32Config.h"

#include "colorlcd_libdsPICVH.h"
#include "sd.h"
#include "adc.h"
#include "serial.h"
#include "gps.h"
#include "port.h"
#include "timer.h"
#include "lcd_touch.h"
#include "digitalMeter.h"
#include "displayControl.h"

//#pragma config FNOSC=FRCPLL, POSCMOD=OFF, FPLLIDIV=DIV_2
//#pragma config FPLLMUL=MUL_20, FPBDIV=DIV_1, FPLLODIV=DIV_2
//#pragma config FNOSC=FRCPLL, POSCMOD=OFF, FPBDIV=DIV_1
//#pragma config FPLLIDIV=DIV_2, FPLLMUL=MUL_20, FPLLODIV=DIV_2
#if 1
#pragma config FNOSC=PRIPLL, POSCMOD=XT, FPBDIV=DIV_1
#pragma config FPLLIDIV=DIV_2, FPLLMUL=MUL_20, FPLLODIV=DIV_1

#pragma config FWDTEN   = OFF
#pragma config DEBUG    = ON
//#pragma config JTAGEN   = OFF
#pragma config OSCIOFNC = OFF
#pragma config FSOSCEN  = OFF
#pragma config ICESEL   = ICS_PGx1
#endif

static void prvSetupHardware(void) {
    /* Configure the hardware for maximum performance. */
    vHardwareConfigurePerformance();

    /* Setup to use the external interrupt controller. */
    vHardwareUseMultiVectoredInterrupts();

    portDISABLE_INTERRUPTS();

}

/*
 * 
 */
int main(int argc, char** argv) {


    sysPerrformanceConfig(80000000L);

    DDPCONbits.JTAGEN = 0;
    
    // デバッグ用ポートの初期化
    DEBUG_PORT1_TRIS = 0;
    DEBUG_PORT1 = 0;
    DEBUG_PORT2_TRIS = 0;
    DEBUG_PORT2 = 0;

    prvSetupHardware();

    // 各モジュール初期化
    uart_initialize();
//    adc_initialize();
	port_initialize();
	InitSpiPort();
    gps_initialize();
    touch_initialize();

    // LCD初期化
    Glcd_Init();
    
    // タイマ開始
    freerun_timer2_initialize();
    freerun_timer3_initialize();

    sdCommInit();
    sevenSegInit();
    JPEGInit();
    
    // 各タスク生成
    create_adc_task();
    create_lcd_touch_task();
    create_gps_parse_task();
    create_lcd_write_task();
    create_lcd_task();
    create_sd_control_task();

    vTaskStartScheduler();
    
    for (;;);

    return (EXIT_SUCCESS);
}



