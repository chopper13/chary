/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     gps_parser.c
 *
 *     GPSパーサ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>
#include <stdbool.h>

#include "gps_parser.h"

/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define VALUE_BUF_CNT_MAX	16

#define GPS_RCV_GPVTG	0x1	/* GPSセンテンス VTG */
#define GPS_RCV_GPGGA	0x2	/* GPSセンテンス GGA */
#define GPS_RCV_GPRMC	0x3	/* GPSセンテンス RMC */


/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* データ受信確認ステータス */
enum {
    GPS_RCV_STS_NONE = 0,
    GPS_RCV_STS_DOLLAR, /* DOLLAR         '$'                   */
    GPS_RCV_STS_HEAD, /* HEAD           'G'|'P'|'R'|'M'|'C'   */

    GPS_RCV_STS_RMC_TIME, /* TIME           '0' - '9', '.'        */
    GPS_RCV_STS_RMC_2, /* RMC 2                                */
    GPS_RCV_STS_RMC_LATITUDE, /* LATITUDE       '0' - '9', '.'        */
    GPS_RCV_STS_RMC_ORIENTATION_N_S, /* ORIENTATION    'S'|'N'               */
    GPS_RCV_STS_RMC_LONGITUDE, /* LONGITUDE      '0' - '9', '.'        */
    GPS_RCV_STS_RMC_ORIENTATION_E_W, /* ORIENTATION    'E'|'W'               */
    GPS_RCV_STS_RMC_SPEED_KNOTS, /* SPEED KNOTS    '0' - '9', '.'        */
    GPS_RCV_STS_RMC_TRACK_TRUE, /* TRACK TRUE     '0' - '9', '.'        */
    GPS_RCV_STS_RMC_DATE, /* DATE           '0' - '9'             */
    GPS_RCV_STS_RMC_MAGNETIC, /* MAGNETIC       '0' - '9', '.'        */
    GPS_RCV_STS_RMC_MAGNETIC_E_W, /* ORIENTATION    'E'|'W'               */
    GPS_RCV_STS_RMC_12, /* RMC 12                               */

    GPS_RCV_STS_ASTERISK, /* ASTERISK       '*'                   */
    GPS_RCV_STS_CHECK_SUM, /* CHECK SUM      '0' - '9', 'A' - 'F'  */
    GPS_RCV_STS_MAX
};

/* RMCセンテンス受信データ */
typedef struct tagRMCSentenceRcvData {
    GPSTime time; /* Time      時間       */
    GPSDegree lat; /* Latitude  緯度       */
    GPSDegree lgn; /* Longitude 経度       */
    float spd; /* Speed     速度       */
    float course; /* Course    進行方向度 */
    GPSDate date; /* Date      日時       */
    GPSMagnetic mag; /* Magnetic  方位       */
} RMCSentenceRcvData;



/* --- 変数宣言 --------------------------------------------------------------*/
GPSData gps_data;

unsigned char gps_rcv_data_state; /* GPS受信データステータス */

unsigned char isGpsRcvEnable; /* GPS受信可能ステータス */

RMCSentenceRcvData rmc_rcv_data; /* RMC受信データ */

char head_buf[6]; /* GPSデータ センテンス種別格納用配列 */
unsigned short head_cnt;

unsigned char SentenceFlg; /* 受信センテンス確認用フラグ VTG or GGA or RMC */

unsigned char value; /* ASCII変換後数値格納用変数 */
unsigned char value_buf[VALUE_BUF_CNT_MAX]; /* 数値データ格納用配列 */
unsigned char value_cnt; /* 格納データカウンタ */
unsigned char decimal_point_num; /* 格納データ小数点位置番号 */

unsigned int sum_val; /* センテンスチェックサム加算格納用変数 */

unsigned char isRMCSentenceRcv; /* RMCセンテンス受信フラグ */

bool isSign; /* 符号フラグ */


const char StrGGA[] = "GPGGA"; /* 文字列比較用 "GPGGA" */
const char StrVTG[] = "GPVTG"; /* 文字列比較用 "GPVTG" */
const char StrRMC[] = "GPRMC"; /* 文字列比較用 "GPRMC" */


const unsigned short PowExpTable10[5] = {1, 10, 100, 1000, 10000};

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
void HeadAnalize(unsigned char head_data); /* Head部解析 */
unsigned char Ascii2HexConv(unsigned char ascii_data); /* ASCII->数値変換 */
unsigned char OrientationAnalize(unsigned char ori_data); /* 方角(N,S,E.W)->0 or 1 */

/* 進行方向値を格納 */
void SetTrueTrack(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, unsigned short* head_res);
/* 進行速度値を格納 */
void SetSpeed(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, unsigned short* speed_res);

/* 小数点ありの数値を格納 */
void SetFloat(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, float* float_res);

/* 測位時刻を格納 */
void SetTime(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, GPSTime* time_res);
/* 緯度,経度を格納 */
void SetGpsDegree(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, GPSDegree* gps_res);
void SetGpsCompass(unsigned char* buf, unsigned char cnt, unsigned char* compass_res);
/* 日付を格納 */
void SetDate(unsigned char* buf, unsigned char cnt, GPSDate* date_res);
/* 測位状態値を格納 */
void SetGPSQuality(unsigned char* buf, unsigned char cnt, unsigned short* cond_res);

/* チェックサム値変換 */
unsigned char CalcCheckSum(unsigned char* buf);

/* モニタ関連変数初期化 */
void MonVariableInitialize(void);


/* --- ローカル関数群 --------------------------------------------------------*/

/*
 *  HeadAnalize
 *   ヘッダ部解析
 *   受信したGPSセンテンスヘッダを確認
 *   以下以外のセンテンスは無視する
 *   VTG
 *   GGA
 *   RMC
 */
void HeadAnalize(unsigned char head_data) {
    head_buf[head_cnt] = head_data;
    head_cnt++;
    if (head_cnt >= 5) {
        if (strcmp(head_buf, StrVTG) == 0) {
            SentenceFlg = GPS_RCV_GPVTG;
        } else if (strcmp(head_buf, StrGGA) == 0) {
            SentenceFlg = GPS_RCV_GPGGA;
        } else if (strcmp(head_buf, StrRMC) == 0) {
            SentenceFlg = GPS_RCV_GPRMC;
        } else {
            gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
        }
        head_cnt = 0;
    }
}

/*
 *  Ascii2HexConv
 *   アスキーコードを数値へ変換
 */
unsigned char Ascii2HexConv(unsigned char ascii_data) {
    unsigned char hex_data;

    switch (ascii_data) {
            /* 数字判断 */
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
            hex_data = ascii_data - '0';
            break;

            /* 16進数数字判断 */
        case 'A': case 'B': case 'C': case 'D': case 'E': case 'F':
            hex_data = ascii_data - 'A' + 10;
            break;

        default:
            hex_data = 0;
            break;
    }

    return hex_data;
}

/*
 *  OrientationAnalize
 *   方角を示す文字をそれぞれ数値の0or1へ変換
 */
unsigned char OrientationAnalize(unsigned char ori_data) {
    unsigned char hex_data;

    switch (ori_data) {
        case 'S': case 'W':
            hex_data = 1;
            break;

        case 'N': case 'E':
        default:
            hex_data = 0;
            break;
    }

    return hex_data;
}

/*
 *  SetTrueTrack
 *   進行方向値を格納
 */
void SetTrueTrack(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num,
        unsigned short* head_res) {
    char n;
    unsigned char pow_exp;

    if (cnt == 0) return;

    pow_exp = 0;

    for (n = cnt - 1; n >= 0; n--) {
        if (n != deci_point_num) {
            *head_res += (buf[n] * PowExpTable10[pow_exp]);
            pow_exp++;
        }
    }
}

/*
 *  SetSpeed
 *   進行速度値を格納
 */
void SetSpeed(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num,
        unsigned short* speed_res) {
    char n;
    unsigned char m;

    if (cnt == 0) return;

    m = 0;
    for (n = cnt - 1; n >= 0; n--) {
        if ((n != deci_point_num) && ((n - deci_point_num) <= 1)) {
            *speed_res |= ((buf[n] & 0x0F) << m);
            m += 4;
        }
    }
}

/* 小数点ありの数値を格納 */
void SetFloat(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, float* float_res) {
    char n, pos;

    if (cnt == 0) return;

    for (n = 0; n < cnt; n++) {
        pos = (char) (n - deci_point_num);
        switch (pos) {
            case -3:
                /* 100桁 */
                *float_res += (float) (buf[n] * 100);
                break;

            case -2:
                /* 10桁 */
                *float_res += (float) (buf[n] * 10);
                break;
            case -1:
                /* 10桁 */
                *float_res += (float) buf[n];
                break;
            case 1:
                /* 0.1桁 */
                *float_res += (float) (buf[n] * 0.1);
                break;

            case 0: /* 小数点位置 */
            default:
                break;
        }
    }
}

/* 測位時刻を格納 */
void SetTime(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, GPSTime* time_res) {
    char n, pos;

    if (cnt == 0) return;

    for (n = 0; n < cnt; n++) {
        pos = (char) (n - deci_point_num);
        switch (pos) {
            case -6:
                /* 時(10) */
                (*time_res).hour += (unsigned char) (buf[n] * 10);
                break;
            case -5:
                /* 時(1) */
                (*time_res).hour += (unsigned char) buf[n];
                break;
            case -4:
                /* 分(10) */
                (*time_res).min += (unsigned char) (buf[n] * 10);
                break;
            case -3:
                /* 分(1) */
                (*time_res).min += (unsigned char) buf[n];
                break;

            case -2:
                /* 秒(10) */
                (*time_res).sec += (unsigned char) (buf[n] * 10);
                break;
            case -1:
                /* 秒(1) */
                (*time_res).sec += (unsigned char) buf[n];
                break;
            case 1:
                /* 秒(0.1) */
                (*time_res).msec += (unsigned short) (buf[n] * 100);
                break;
            case 2:
                /* 秒(0.01) */
                (*time_res).msec += (unsigned short) (buf[n] * 10);
                break;

            case 0: /* 小数点位置 */
            default:
                break;
        }
    }
}

/* 緯度,経度を格納 */
void SetGpsDegree(unsigned char* buf, unsigned char cnt, unsigned char deci_point_num, GPSDegree* gps_res) {
    char n, pos;

    if (cnt == 0) return;

    for (n = 0; n < cnt; n++) {
        pos = (char) (n - deci_point_num);
        switch (pos) {
            case -5:
                /* 度(100) */
                (*gps_res).degree += (unsigned short) (buf[n] * 100);
                break;
            case -4:
                /* 度(10) */
                (*gps_res).degree += (unsigned short) (buf[n] * 10);
                break;
            case -3:
                /* 度(1) */
                (*gps_res).degree += (unsigned short) buf[n];
                break;

            case -2:
                /* 分(10) */
                (*gps_res).min += (unsigned char) (buf[n] * 10);
                break;
            case -1:
                /* 分(1) */
                (*gps_res).min += (unsigned char) buf[n];
                break;
            case 1:
                /* 分(0.1) */
                (*gps_res).minPoint += (unsigned int) (buf[n] * 10000);
                break;

            case 2:
                /* 分(0.01) */
                (*gps_res).minPoint += (unsigned int) (buf[n] * 1000);
                break;
            case 3:
                /* 分(0.001) */
                (*gps_res).minPoint += (unsigned int) (buf[n] * 100);
                break;
            case 4:
                /* 分(0.0001) */
                (*gps_res).minPoint += (unsigned int) (buf[n] * 10);
                break;
            case 5:
                /* 分(0.00001) */
                (*gps_res).minPoint += (unsigned int) buf[n];
                break;

            case 0: /* 小数点位置 */
            default:
                break;
        }
    }
}

/* 経度(N/S),経度(E/W)を格納 */
void SetGpsCompass(unsigned char* buf, unsigned char cnt, unsigned char* compass_res) {
    if (cnt == 0) return;

    /* 経度,緯度1 */
    compass_res += (unsigned char) buf[0];
}

/* 日付を格納 */
void SetDate(unsigned char* buf, unsigned char cnt, GPSDate* date_res) {
    if (cnt == 0) return;

    /* 日 */
    (*date_res).day += (unsigned char) (buf[0] * 10);
    (*date_res).day += (unsigned char) buf[1];

    /* 月 */
    (*date_res).month += (unsigned char) (buf[2] * 10);
    (*date_res).month += (unsigned char) buf[3];

    /* 年 */
    (*date_res).year += (unsigned char) (buf[4] * 10);
    (*date_res).year += (unsigned char) buf[5];
}

/*
 *  CalcCheckSum
 *   チェックサム値変換
 */
unsigned char CalcCheckSum(unsigned char* buf) {
    return ((buf[0] * 16) + (buf[1]));
}

/* モニタ関連変数初期化 */
void MonVariableInitialize(void) {
    gps_rcv_data_state = GPS_RCV_STS_DOLLAR;

    isRMCSentenceRcv = 0;

    isSign = false;
}


/* --- グローバル関数群 ------------------------------------------------------*/

/*
 *  GPS パーサ 初期化
 */
void app_gps_comm_init(void) {
    isGpsRcvEnable = false;

    /* モニタ関連変数初期化 */
    MonVariableInitialize();
}

/*
 *  GPS パーサ 受信データ格納
 */
void gps_parser(unsigned char data) {
    unsigned short n = 0;

    /* ステータス遷移とASCII/HEXの変換*/
    switch (data) {
            /*
             * センテンスの先頭を確認
             * '$'受信でセンテンスの先頭とみなす
             */
        case '$':
            head_cnt = 0;
            sum_val = 0;
            SentenceFlg = 0;
            value = 0;
            for (n = 0; n < VALUE_BUF_CNT_MAX; n++) {
                value_buf[n] = 0;
            }
            value_cnt = 0;
            decimal_point_num = 0;
            isSign = false;
            gps_rcv_data_state = GPS_RCV_STS_HEAD; /* HEAD */
            break;

            /*
             * カンマ判断
             * ','受信でセンテンス内のデータ区切りとみなす
             * ステータス遷移
             */
        case ',':
            sum_val ^= data;
            switch (gps_rcv_data_state) {
                case GPS_RCV_STS_DOLLAR:
                    /* '$' */
                    break;

                case GPS_RCV_STS_HEAD:
                    /* GPxxx */
                    if (SentenceFlg == GPS_RCV_GPVTG) {
                    } else if (SentenceFlg == GPS_RCV_GPGGA) {
                    } else if (SentenceFlg == GPS_RCV_GPRMC) {
                        gps_rcv_data_state = GPS_RCV_STS_RMC_TIME;
                        rmc_rcv_data.time.hour = 0;
                        rmc_rcv_data.time.min = 0;
                        rmc_rcv_data.time.sec = 0;
                        rmc_rcv_data.time.msec = 0;
                        rmc_rcv_data.lat.compass = 0;
                        rmc_rcv_data.lat.degree = 0;
                        rmc_rcv_data.lat.min = 0;
                        rmc_rcv_data.lat.minPoint = 0;
                        rmc_rcv_data.lgn.compass = 0;
                        rmc_rcv_data.lgn.degree = 0;
                        rmc_rcv_data.lgn.min = 0;
                        rmc_rcv_data.lgn.minPoint = 0;
                        rmc_rcv_data.spd = 0.0;
                        rmc_rcv_data.course = 0.0;
                        rmc_rcv_data.date.year = 0;
                        rmc_rcv_data.date.month = 0;
                        rmc_rcv_data.date.day = 0;
                        rmc_rcv_data.mag.compass = 0;
                        rmc_rcv_data.mag.course = 0.0;
                    } else {
                        gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
                    }
                    break;

                    /* RMC */
                case GPS_RCV_STS_RMC_TIME:
                    /* -時間- */
                    SetTime(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.time));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_2;
                    break;

                case GPS_RCV_STS_RMC_2:
                    /* --- */
                    gps_rcv_data_state = GPS_RCV_STS_RMC_LATITUDE;
                    break;

                case GPS_RCV_STS_RMC_LATITUDE:
                    /* -緯度- */
                    SetGpsDegree(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.lat));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_ORIENTATION_N_S;
                    break;

                case GPS_RCV_STS_RMC_ORIENTATION_N_S:
                    /* -緯度- */
                    SetGpsCompass(value_buf,
                            value_cnt,
                            &(rmc_rcv_data.lat.compass));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_LONGITUDE;
                    break;

                case GPS_RCV_STS_RMC_LONGITUDE:
                    /* -経度- */
                    SetGpsDegree(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.lgn));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_ORIENTATION_E_W;
                    break;

                case GPS_RCV_STS_RMC_ORIENTATION_E_W:
                    /* -経度- */
                    SetGpsCompass(value_buf,
                            value_cnt,
                            &(rmc_rcv_data.lgn.compass));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_SPEED_KNOTS;
                    break;

                case GPS_RCV_STS_RMC_SPEED_KNOTS:
                    /* -速度- */
                    SetFloat(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.spd));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_TRACK_TRUE;
                    break;

                case GPS_RCV_STS_RMC_TRACK_TRUE:
                    /* -進行方向- */
                    SetFloat(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.course));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_DATE;
                    break;

                case GPS_RCV_STS_RMC_DATE:
                    /* -日付- */
                    SetDate(value_buf,
                            value_cnt,
                            &(rmc_rcv_data.date));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_MAGNETIC;
                    break;

                case GPS_RCV_STS_RMC_MAGNETIC:
                    /* -方位- */
                    SetFloat(value_buf,
                            value_cnt,
                            decimal_point_num,
                            &(rmc_rcv_data.mag.course));
                    gps_rcv_data_state = GPS_RCV_STS_RMC_MAGNETIC_E_W;
                    break;

                case GPS_RCV_STS_RMC_MAGNETIC_E_W:
                    /* -方位- */
                    SetGpsCompass(value_buf,
                            value_cnt,
                            &(rmc_rcv_data.mag.compass));
                    gps_rcv_data_state = GPS_RCV_STS_ASTERISK;
                    break;

                case GPS_RCV_STS_RMC_12:
                    /* --- */
                    gps_rcv_data_state = GPS_RCV_STS_ASTERISK;
                    break;

                case GPS_RCV_STS_ASTERISK:
                case GPS_RCV_STS_CHECK_SUM:
                    break;

                default:
                    gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
                    isRMCSentenceRcv = 0;
                    SentenceFlg = 0;
                    break;
            }
            for (n = 0; n < VALUE_BUF_CNT_MAX; n++) {
                value_buf[n] = 0;
            }
            value_cnt = 0;
            decimal_point_num = 0;
            isSign = false;
            break;

            /*
             * アスタリスク判断
             * '*'受信でチェックサムステータスへ遷移
             */
        case '*':
            if (gps_rcv_data_state == GPS_RCV_STS_ASTERISK) {
                gps_rcv_data_state = GPS_RCV_STS_CHECK_SUM;
                for (n = 0; n < VALUE_BUF_CNT_MAX; n++) {
                    value_buf[n] = 0;
                }
                value_cnt = 0;
                decimal_point_num = 0;
            } else {
                gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
                isRMCSentenceRcv = 0;
                SentenceFlg = 0;
            }
            break;

            /*
             *  改行コード判断
             *  '\r' CR:キャリッジリターン
             */
        case '\r':
            if (gps_rcv_data_state == GPS_RCV_STS_CHECK_SUM) {
                /* チェックサム計算 */

                // チェックサム
                if ((unsigned char) sum_val == CalcCheckSum(value_buf)) {
                    if (SentenceFlg == GPS_RCV_GPRMC) {
                        isRMCSentenceRcv = 1;
                    } else {
                        gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
                    }
                } else {
                    isRMCSentenceRcv = 0;
                    SentenceFlg = 0;

                    for (n = 0; n < VALUE_BUF_CNT_MAX; n++) {
                        value_buf[n] = 0;
                    }
                    value_cnt = 0;
                }

                /*
                 *  RMCセンテンスのデータ受信が出来たらデータをバッファへ格納する
                 */
                if ((isRMCSentenceRcv == 1)) {
                    if (isRMCSentenceRcv == 1) {
                        /* --- GPRMC --- */
                                                // 日本時間に変換
                        rmc_rcv_data.time.hour += 9;
                        if (24 <= rmc_rcv_data.time.hour) {
                            rmc_rcv_data.time.hour -= 24;
                        }

                        // ノットをキロに変換
                        rmc_rcv_data.spd *= 1.852;

                        // 年を4桁に変換
                        rmc_rcv_data.date.year += 2000;
                        
                        gps_data.time = rmc_rcv_data.time;
                        gps_data.lat = rmc_rcv_data.lat;
                        gps_data.lgn = rmc_rcv_data.lgn;
                        gps_data.spd = rmc_rcv_data.spd;
                        gps_data.course = rmc_rcv_data.course;
                        gps_data.date = rmc_rcv_data.date;
                        gps_data.mag = rmc_rcv_data.mag;

                        isRMCSentenceRcv = 0;
                    }

                    SentenceFlg = 0;
                }
            }
            gps_rcv_data_state = GPS_RCV_STS_DOLLAR;
            break;

            /*
             * ドット判断
             * '.'受信
             * Hex:0x2e(46)
             */
        case '.':
            sum_val ^= data;
            value_buf[value_cnt] = data;
            decimal_point_num = value_cnt;
            if (value_cnt < (VALUE_BUF_CNT_MAX - 1)) {
                value_cnt++;
            }
            break;

            /* 符号判断 */
        case '-':
            sum_val ^= data;
            isSign = true;
            break;

            /* 数字判断 */
        case '0': case '1': case '2': case '3': case '4':
        case '5': case '6': case '7': case '8': case '9':
        case 'A': case 'B': case 'C': case 'D': case 'F':
            if (gps_rcv_data_state != GPS_RCV_STS_CHECK_SUM) {
                sum_val ^= data;
            }

            if (gps_rcv_data_state == GPS_RCV_STS_HEAD) {
                HeadAnalize(data);
            } else {
                value_buf[value_cnt] = Ascii2HexConv(data);
                if (value_cnt < (VALUE_BUF_CNT_MAX - 1)) {
                    value_cnt++;
                }
            }
            break;

            /* 北緯・南緯・東経・西経を示す、N/S/E/Wを変換 */
        case 'N': case 'S': case 'W':
            sum_val ^= data;
            value_buf[value_cnt] = OrientationAnalize(data);
            if (value_cnt < (VALUE_BUF_CNT_MAX - 1)) {
                value_cnt++;
            }
            break;

        case 'E':
            if (gps_rcv_data_state != GPS_RCV_STS_CHECK_SUM) {
                sum_val ^= data;
            }
#if 0
            if (gps_rcv_data_state == GPS_RCV_STS_GGA_ORIENTATION_E_W) {
                value_buf[value_cnt] = OrientationAnalize(data);
                if (value_cnt < (VALUE_BUF_CNT_MAX - 1)) value_cnt++;
            } else
#endif
            {
                value_buf[value_cnt] = Ascii2HexConv(data);
                if (value_cnt < (VALUE_BUF_CNT_MAX - 1)) {
                    value_cnt++;
                }
            }
            break;

        default:
            sum_val ^= data;
            /*
             * ステータスがGPS_RCV_STS_HEADの場合は
             * 受信したセンテンス種別を確認
             */
            if (gps_rcv_data_state == GPS_RCV_STS_HEAD) {
                HeadAnalize(data);
            }
            break;
    }
}

