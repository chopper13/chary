/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     app_gps_comm.c
 *
 *     GPSコミュニケーション アプリ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/
#include <xc.h>

#include "FreeRTOS.h"
#include "task.h"
#include "gps_parser.h"

#include "gps.h"
#include "serial.h"


/* --- 定数・マクロ定義 ------------------------------------------------------*/
#define TASK_PERIOD_MSEC	100		// 定期取得タスク周期
#define TASK_PRIORITY		1		// タスク優先度
#define TASK_STACK_SIZE		192		// スタックサイズ

#define PARSE_BUF_SIEZE		64

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/

/* --- ローカル関数群 --------------------------------------------------------*/
static void gps_parse_task(void *pvParameters) {
    TickType_t xNextWakeTime = xTaskGetTickCount();

    for (;;) {
        while (uart_gps_rcv_buf_empty() == 0) {
            gps_parser(uart_gps_rcv_buf_getc());
        }

        vTaskDelayUntil(&xNextWakeTime, TASK_PERIOD_MSEC / portTICK_RATE_MS);
    }
}

/* --- グローバル関数群 ------------------------------------------------------*/
void gps_initialize(void) {
    const char interval_setting[16] = {
        0xA0,
        0xA1,
        0x00,
        0x09,
        0x08, // Configure NMEA message interval 
        0x00, // GGA
        0x00, // GSA
        0x00, // GSV
        0x00, // GLL
        0x01, // RMC
        0x00, // VTG
        0x00, // ZDA
        0x00, // ATTR
        0x00, // Parity
        0x0D,
        0x0A
    };

    uart_gps_snd_buf_puts((char*) interval_setting, 16);
    app_gps_comm_init();
}

void create_gps_parse_task(void) {
    BaseType_t ret;

    ret = xTaskCreate(gps_parse_task, "gps", TASK_STACK_SIZE, NULL,
            TASK_PRIORITY, NULL);
    if (ret != pdPASS) {
        // タスク生成失敗
        uart_debug_snd_buf_puts("create_gps_parse_task failed\r\n");
    }
}

int get_gps_year(void) {
    return gps_data.date.year;
}

int get_gps_mon(void) {
    return gps_data.date.month;
}

int get_gps_day(void) {
    return gps_data.date.day;
}

int get_gps_hour(void) {
    return gps_data.time.hour;
}

int get_gps_min(void) {
    return gps_data.time.min;
}

int get_gps_sec(void) {
    return gps_data.time.sec;
}

int get_gps_hsec(void) {
    return gps_data.time.msec;
}

float get_gps_speed(void) {
    return gps_data.spd;
}

float get_gps_declination(void) {
    return gps_data.course;
}

int get_gps_lat_degree(void) {
    return gps_data.lat.degree;
}

int get_gps_lat_min(void) {
    return gps_data.lat.min;
}

int get_gps_lon_degree(void) {
    return gps_data.lgn.degree;
}

int get_gps_lon_min(void) {
    return gps_data.lgn.min;
}

