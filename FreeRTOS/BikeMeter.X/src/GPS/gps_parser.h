/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     gps_parser.h
 *
 *     GPSパーサ ヘッダ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef __GPS_PARSER_H_
#define __GPS_PARSER_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/
typedef struct tagGPSDegree {
    unsigned char compass;
    unsigned short degree;
    unsigned char min;
    unsigned int minPoint;
} GPSDegree;

typedef struct tagGPSTime {
    unsigned char hour;
    unsigned char min;
    unsigned char sec;
    unsigned short msec;
} GPSTime;

typedef struct tagGPSDate {
    unsigned short year;
    unsigned char month;
    unsigned char day;
} GPSDate;

typedef struct tagGPSMagnetic {
    unsigned char compass;
    float course;
} GPSMagnetic;

typedef struct tagGPSData {
    GPSTime time; /* Time      時間       */
    GPSDegree lat; /* Latitude  緯度       */
    GPSDegree lgn; /* Longitude 経度       */
    float spd; /* Speed     速度       */
    float course; /* Course    進行方向度 */
    GPSDate date; /* Date      日時       */
    GPSMagnetic mag; /* Magnetic  方位       */
} GPSData;

/* --- 変数宣言 --------------------------------------------------------------*/
extern GPSData gps_data;

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void app_gps_comm_init(void);
extern void gps_parser(unsigned char data);

#endif /* __GPS_PARSER_H_ */
