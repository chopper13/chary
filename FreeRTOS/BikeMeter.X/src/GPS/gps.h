/* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
 *     gps.h
 *
 *     GPS関連 ヘッダ
 *
 *
 *
 *
 * - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

#ifndef __GPS_H_
#define __GPS_H_

/* --- 外部ヘッダインクルードファイル ----------------------------------------*/

/* --- 定数・マクロ定義 ------------------------------------------------------*/

/* --- 構造体・共用体定義 ----------------------------------------------------*/

/* --- 変数宣言 --------------------------------------------------------------*/

/* --- テーブル定義 ----------------------------------------------------------*/

/* --- 関数宣言 --------------------------------------------------------------*/
extern void gps_initialize(void);
extern void create_gps_parse_task(void);
extern int get_gps_year(void);
extern int get_gps_mon(void);
extern int get_gps_day(void);
extern int get_gps_hour(void);
extern int get_gps_min(void);
extern int get_gps_sec(void);
extern int get_gps_hsec(void);
extern int get_gps_lat_degree(void);
extern int get_gps_lat_min(void);
extern int get_gps_lon_degree(void);
extern int get_gps_lon_min(void);
extern float get_gps_speed(void);
extern float get_gps_declination(void);

#endif /* __GPS_H_ */
