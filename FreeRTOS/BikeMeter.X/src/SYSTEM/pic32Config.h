#ifndef PIC32CONFIG_H
#define PIC32CONFIG_H

#include <xc.h>

/*      pic32のデバイス仕様（将来的には変更になる可能性も）
 */
#define         SYS_DEVCON_PIC32MX_MAX_PB_FREQ  80000000UL
#define         ROM_0WAIT_FREQ                                  30000000Ul
#define         ROM_1WAIT_FREQ                                  60000000Ul


/*      割り込み管理
 */
#define DI()            __builtin_disable_interrupts()

//　GI()マクロはもう少し端的な記述が見つかったので変更します。
//　GIDI()マクロはマクロの引数に結果を返すのは他のマクロとの整合性が悪いので変更します。
#define GI()            (_CP0_GET_STATUS() & 0x01)  //0x01はIE bitの位置
#define GIDI()          GI();DI()

#define EI()            __builtin_enable_interrupts()

#define RI(flag)        (flag) ?        __builtin_enable_interrupts() : \
                                        __builtin_disable_interrupts()


extern void sysPerrformanceConfig( unsigned int sysclk );

#endif  //PIC32CONFIG_H
