#include "pic32Config.h"

void sysPerrformanceConfig( unsigned int sysclk )
{
        //キャッシュを有効にする（キャッシュ機能の無いCPUでは削除する）
        /*      キャッシュの管理はCPUではなくCP0にある。CP0のレジスタ16セレクト0
         *      ConfigRegのbit0〜2にあるK0レジスタがそれ。
         *      詳細はPIC32日本語マニュアルセクション2「M4Kコア搭載デバイス用CPU」
         */
        register unsigned long tmp;
        asm("mfc0 %0,$16,0" :  "=r"(tmp));
        tmp = (tmp & ~7) | 3;                                   //2:キャッシュ無効, 3:有効
        asm("mtc0 %0,$16,0" :: "r" (tmp));
        
        //ROMへアクセスするときのウェイト数
        int wait;
        if(sysclk <= ROM_0WAIT_FREQ)            wait = 0;
        else if (sysclk <= ROM_1WAIT_FREQ)      wait = 1;
        else                                    wait = 2;
        CHECONbits.PFMWS = wait;
        
        //RAMへアクセスするときのウェイト数
        BMXCONbits.BMXWSDRM = 0;

        //PBDIVの設定
        int pbdiv = (sysclk > SYS_DEVCON_PIC32MX_MAX_PB_FREQ) ? 2 : 1;
        OSCCONbits.PBDIV = pbdiv;
        
}
